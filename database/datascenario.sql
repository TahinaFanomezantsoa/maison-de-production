/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/SQLTemplate.sql to edit this template
 */
/**
 * Author:  charles
 * Created: 21 mars 2023
 */


create database film;

\c film postgres;


create sequence plateau_seq start 50 increment 1;
create sequence personne_seq start 50 increment 1;
create sequence scene_seq start 50 increment 1;
create sequence scenepersonnage_seq start 50 increment 1;
create sequence emotion_seq start 50 increment 1;
create sequence disponibiliteplateau_seq start 50 increment 1;
create sequence disponibilitepersonnage_seq start 50 increment 1;
create sequence heuretravailjour_seq start 1 increment 1;
create sequence heuretravailsemaine_seq start 1 increment 1;
create sequence heuretravailannee_seq start 1 increment 1;

create table heuretravailjour (
    id text primary key,
    debut time,
    fin time
);

create table heuretravailsemaine (
    id text primary key,
    joursemaine integer 
);

create table heuretravailannee (
    id text primary key,
    date date
);

create table personne (
    id text primary key,
    noms text not null,
    identifiant text unique not null,
    motdepasse text,
    typepersonne text
);

create table personnage 
(
    id text primary key,
    noms text
);

create table emotion 
(
    id text primary key,
    libelle text
);

create table plateau 
(
    id text primary key,
    libelle text,
    description text
);

create table configurationcontenu (
    nombrecontenuparpage integer
);

create table configurationcontenuplannification (
    nombrecontenuparpage integer
);

create table jourtravail
(
    id text primary key,
    debut time not null,
    fin time not null
);

create table disponibiliteplateau 
(
    id text primary key,
    idplateau text references plateau (id), 
    datedebut timestamp default null,
    datefin timestamp default null
    
);

create table disponibilitepersonnage
(
    id text primary key,
    idpersonnage text references personnage (id), 
    datedebut timestamp default null,
    datefin timestamp default null 
);

create table scene 
(
    id text primary key,
    libelle text,
    idpersonne text references personne (id),
    idplateau text references plateau (id),
    tempsdebut time,
    tempsfin time,
    datevalidation timestamp default null,
    etat integer default 1,
    ordre integer unique,
    datedebutplannification timestamp default null,
    datefinplannification timestamp default null
);

create table scenepersonnage
(
    id text primary key,
    idscene text references scene (id),
    idpersonnage text references personnage (id),
    dialogue text,
    action text,
    idemotion text references emotion (id),
    tempsdebut time,
    tempsfin time
);

create or replace view sceneplanifie as select * from scene where datevalidation is not null order by datedebutplannification;


insert into jourtravail values('jt1', '08:00','17:00');

insert into configurationcontenu values (3);

insert into configurationcontenuplannification values (10);





-- scenario data 
    -- personne
        insert into personne values ('pers1', 'JEAN JEAN','admin', 'admin', 'admin');
        insert into personne values ('pers2', 'MARIE MARIE','admin1', 'admin1', 'admin');

    -- plateau
        insert into plateau values ('pt1', 'Morarano', 'ao aminy tranony Mandresy');
        insert into plateau values ('pt2', 'Mandrimena', 'ao aminy ITU');
    
    -- personnage
        insert into personnage values ('p1','Cedric');
        insert into personnage values ('p2','Midera');
        insert into personnage values ('p3','Murphy');
        insert into personnage values ('p4','Idealy');
        insert into personnage values ('p5','Babouche');
        insert into personnage values ('p6','Joy Dad');
        insert into personnage values ('p7','Mitasoa');
        insert into personnage values ('p8','Dina');
        insert into personnage values ('p9','Mino');
        insert into personnage values ('p10','Bandy am s4');
        insert into personnage values ('p11','Hery Niavo');
        insert into personnage values ('p12','Miora');
-- scene
            --id|libelle|idpersonne|idplateau|tempsdebut|tempsfin|datevalidation|etat|datedebutplannification|datefinplannification
        insert into scene values ('s1', 'I Cedric matory am Midera', 'pers1', 'pt1', '00:14', '00:30', now(), '21', '1', null, null);  
            -- emotion 
                insert into emotion values ('e1','fatigué');
                insert into emotion values ('e2','étonné');
            -- scenepersonnage
                --id |idscene|idpersonnage|dialogue|action|idemotion|tempsdebut|tempsfin--

                insert into scenepersonnage values ('sp1', 's1', 'p1', null, 'matory ambon ny fean i Midera', 'e1', '00:14', '00:30');
                insert into scenepersonnage values ('sp2', 's1', 'p2', null, 'mijery ny manodidina', 'e2', '00:14', '00:15');
        
        insert into scene values ('s2', 'I Midera manafosafo lohan''i Cedric', 'pers1', 'pt1', '00:31', '01:15', now(), '21', '2', null, null);
            -- emotion 
                insert into emotion values ('e3','en train de dormir');
                insert into emotion values ('e4','très content');
            -- scenepersonnage
                insert into scenepersonnage values ('sp3', 's2', 'p1', null, 'matory ambon ny fean i Midera', 'e3', '00:31', '01:15');
                insert into scenepersonnage values ('sp4', 's2', 'p2', 'qu il est mignon mon bobonin...', 'manafosafo an Cedric', 'e4', '00:31', '00:32');
    
        insert into scene values ('s3', 'Murphy manao informatique', 'pers1', 'pt1', '01:16', '01:30', now(), '21', '3', null, null);
            -- emotion 
                insert into emotion values ('e5','très concentré');
            -- scenepersonnage
                insert into scenepersonnage values ('sp5', 's3', 'p3', null, 'mikitikitika ordinateur', 'e5', '01:16', '01:30');

        insert into scene values ('s4', 'Idealy mitsangana ambony seza', 'pers1', 'pt1', '01:31', '01:45', now(), '21', '4', null, null);
            -- scenepersonnage
                insert into scenepersonnage values ('sp6', 's4', 'p4', 'Aiza eh! jereo nge za eh!', 'mitsangana ambony seza', 'e4', '01:31', '01:45');

        insert into scene values ('s5', 'Babouche mipetraka eo akaikin''i Murphy', 'pers1', 'pt1', '01:46', '01:50', now(), '21', '5', null, null);
            -- scenepersonnage
                insert into scenepersonnage values ('sp7', 's5', 'p5', 'Rodolph letie', 'mipetraka eo akaikin''i Murphy', 'e4', '01:47', '01:48');
                insert into scenepersonnage values ('sp8', 's5', 'p3', 'In eh?', 'mianatra eo amin ny ordinateur', 'e5', '01:46', '01:50');

        insert into scene values ('s6', 'Babouche manazava lesona', 'pers2', 'pt2', '01:51', '02:00', now(), '21', '6', null, null);
            -- emotion 
                insert into emotion values ('e6','très explicatif');
            -- scenepersonnage
                insert into scenepersonnage values ('sp9', 's6', 'p5', 'tsy mety io raha zao no itondranla anazy', null, 'e6', '01:55', '02:00');

        insert into scene values ('s7', 'Babouche manome totohondry rindrina', 'pers2', 'pt2', '01:51', '02:00', now(), '21', '7', null, null);
            -- emotion 
                insert into emotion values ('e7','très fâché');
            -- scenepersonnage
                insert into scenepersonnage values ('sp10', 's7', 'p5', 'sarotra atoro tsssss', 'totohondry rindrina', 'e7', '01:55', '02:00');

        insert into scene values ('s8', 'Misy souris latsaka', 'pers2', 'pt2', '02:31', '02:00', now(), '21', '8', null, null);

        insert into scene values ('s9', 'Joy Dad nitsangana', 'pers2', 'pt2', '02:01', '02:30', now(), '21', '9', null, null);
            -- scenepersonnage
                insert into scenepersonnage values ('sp11', 's9', 'p6', null, 'mitsangana', null, '02:21', '02:30');

        insert into scene values ('s10', 'Mitasoa manara maso ny asa ataon''i Dina', 'pers2', 'pt1', '02:31', '02:41', now(), '21', '10', null, null);
            -- scenepersonnage
                insert into scenepersonnage values ('sp12', 's10', 'p7', 'manara maso ny asa ataon''i Dina', null, 'e5', '02:35', '02:41');
                insert into scenepersonnage values ('sp13', 's10', 'p8', 'mikitika ordinateur', null, 'e5', '02:31', '02:41');

        insert into scene values ('s11', 'Mitasoa mihomehy mafy be', 'pers1', 'pt2', '02:42', '03:42', now(), '21', '11', null, null);
            -- scenepersonnage
                insert into scenepersonnage values ('sp14', 's11', 'p7', 'ahanhan', 'mihomehy', 'e4', '03:40', '03:42');

        insert into scene values ('s12', 'Mino nangataka mouchoir tamin''ny bandy am s4', 'pers1', 'pt2', '03:43', '03:50', now(), '21', '12', null, null);
            -- emotion 
                insert into emotion values ('e8','très demandant');
                insert into emotion values ('e9','un peu étonné');
            -- scenepersonnage
                insert into scenepersonnage values ('sp15', 's12', 'p9', 'tairo mba omeo mouchoir', null, 'e8', '03:43', '03:44');
                insert into scenepersonnage values ('sp16', 's12', 'p10', 'hein?', null, 'e9', '03:43', '03:44');

        insert into scene values ('s13', 'Mino nivoaka ny fenarana', 'pers1', 'pt2', '03:51', '03:59', now(), '21', '13', null, null);
            -- scenepersonnage
                insert into scenepersonnage values ('sp17', 's13', 'p9', null, 'mivoaka ny fenarana', 'e4', '03:51', '03:52');

        insert into scene values ('s14', 'Hery Niavo milalao lohan''i Mitasoa', 'pers1', 'pt2', '04:00', '04:11', now(), '21', '14', null, null);
            -- emotion 
                insert into emotion values ('e10','air draguant');
            -- scenepersonnage
                insert into scenepersonnage values ('sp18', 's14', 'p11', 'asandratro kely aloha an, lohany fotsiny', 'milalao lohan''i Mitasoa', 'e10', '04:09', '04:11');
                insert into scenepersonnage values ('sp19', 's14', 'p7', null, null, 'e5', '04:00', '04:11');

        insert into scene values ('s15', 'Mitasoa miresaka amin''i Miora', 'pers1', 'pt2', '04:12', '04:24', now(), '21', '15', null, null);
            -- scenepersonnage
                insert into scenepersonnage values ('sp20', 's15', 'p7', 'de ahoana akia le izy an', 'miresaka amin''i Miora', 'e5', '04:12', '04:13');
                insert into scenepersonnage values ('sp21', 's15', 'p12', 'le in zany', 'mamaly amin''i Mitasoa', 'e5', '04:13', '04:14');

        insert into scene values ('s16', 'Mitasoa nihomehy', 'pers2', 'pt1', '04:25', '04:30', now(), '21', '16', null, null);
            -- scenepersonnage
                insert into scenepersonnage values ('sp22', 's16', 'p7', 'Asandrato ny tsy fitadidiana eh', 'mihomehy', 'e4', '04:25', '04:26');

        insert into scene values ('s17', 'Mino niditra ny fenarana', 'pers2', 'pt2', '04:31', '04:53', now(), '21', '17', null, null);
            -- scenepersonnage
                insert into scenepersonnage values ('sp23', 's17', 'p9', null, 'miditra ny fenarana', null, '04:31', '04:32');

        insert into scene values ('s18', 'Mino namerina ny ambin''ny mouchoir n''ilay bandy s4', 'pers2', 'pt2', '00:14', '00:30', now(), '21', '18', null, null);
            -- scenepersonnage
                insert into scenepersonnage values ('sp24', 's18', 'p9', 'merci', 'namerina ny ambin''ny mouchoir n''ilay bandy s4', 'e4', '00:14', '00:15');
                insert into scenepersonnage values ('sp25', 's18', 'p10', null, 'manaiky amin''ny loha', 'e4', '00:15', '00:16');

        insert into scene values ('s19', 'Idealy mihomehy', 'pers2', 'pt1', '04:54', '05:04', now(), '21', '19', null, null);
            -- scenepersonnage
                insert into scenepersonnage values ('sp26', 's19', 'p4', 'hahahahaha', 'mihomehy', 'e4', '04:54', '05:55');

        insert into scene values ('s20', 'Idealy milalao usb an''i Mitasoa', 'pers2', 'pt1', '05:05', '05:21', now(), '21', '20', null, null);
            -- emotion 
                insert into emotion values ('e11','fou');
            -- scenepersonnage
                insert into scenepersonnage values ('sp27', 's20', 'p4', 'hafahafa koa zany cle-anareo zany', 'milalao usb an''i Mitasoa', 'e11', null, null); ---ITO LE IS

        insert into scene values ('s21', 'Hery Niavo sy Idealy mijery ny ataon''i Mitasoa', 'pers1', 'pt1', '05:22', '05:28', now(), '21', '21', null, null);
            -- scenepersonnage
                insert into scenepersonnage values ('sp28', 's21', 'p11', null, 'mijery ny ataon''i Mitasoa', 'e5', '05:23', '05:28');
                insert into scenepersonnage values ('sp29', 's21', 'p4', 'Eh!', null, 'e11', '05:22', '05:22');
                insert into scenepersonnage values ('sp30', 's21', 'p7', null, 'mijery ny ataon''i Mitasoa', 'e5', '05:23', '05:28');

        insert into scene values ('s22', 'Niditra ny fenarana i Joy Dad', 'pers1', 'pt1', '05:29', '05:36', now(), '21', '22', null, null);
            -- scenepersonnage
                insert into scenepersonnage values ('sp31', 's22', 'p6', null, 'miditra ny fenarana ',null, '05:29', '05:30');

        insert into scene values ('s23', 'Le bandy s4 niresaka tamin''i Mitasoa', 'pers1', 'pt1', '05:37', '05:54', now(), '21', '23', null, null);
            -- scenepersonnage    
                insert into scenepersonnage values ('sp32', 's23', 'p10', 'misy zavatra tsy azoko lty eh', 'miresaka amin''i Mitasoa', 'e5', '05:37', '05:38');
                insert into scenepersonnage values ('sp33', 's23', 'p7', 'in zany', 'mamaly', 'e5', '05:38', '05:39');

        insert into scene values ('s24', 'Le bandy s4 nivoaka ny fenarana', 'pers1', 'pt1', '05:55', '06:01', now(), '21', '24', null, null);
            -- scenepersonnage    
                insert into scenepersonnage values ('sp34', 's24', 'p10', 'raims aloha fa poritra ah', 'mivoaka ny fenarana', null, '05:55', '05:56');

        insert into scene values ('s25', 'Idealy mitabataba momban''ny disque dur', 'pers1', 'pt1', '06:02', '06:20', now(), '21', '25', null, null);
            -- scenepersonnage
                insert into scenepersonnage values ('sp35', 's25', 'p4', 'ahoana koa ary tony disque dur tony', 'mitabataba', 'e11', '06:02', '06:04');
        
   
        -- plannification : 2023-03-21 20:00 à 2023-05-12 07:00 
        -- scenario : scenario 1 : I Cedric matory am Midera
        --                     2 : I Midera manafosafo lohan'i Cedric

        --plannification  : 2023-03-31 06:00:00 à 2023-04-04 18:00:00 (zoma)
                        
        --            scenario 3 : Murphy manao informatique
        --            scenario 4 : Idealy mitsangana ambony seza

        --plannification  : 2023-04-08 2023-04-12 (test jour ferie)paque
        --            scenario 5 : Babouche manazava lesona
        
        insert into heuretravailjour values ('-- htj1','00:00:00','08:00:00');
        insert into heuretravailjour values ('htj2','18:00:00','00:00:00');

        insert into heuretravailannee values ('hta1','2023-03-29');
        insert into heuretravailannee values ('hta2','2023-04-10');
        

        insert into disponibiliteplateau values ('displat1', 'pt1', '2023-03-21 08:00:00', '2023-03-21 09:30:00');
        insert into disponibiliteplateau values ('displat2', 'pt1', '2023-03-22 09:30:00', '2023-03-22 10:00:00');
        insert into disponibiliteplateau values ('displat3', 'pt1', '2023-03-23 10:00:00', '2023-03-23 11:00:00');
        insert into disponibiliteplateau values ('displat4', 'pt1', '2023-03-24 08:00:00', '2023-03-24 18:00:00');


        insert into disponibiliteplateau values ('displat5', 'pt2', '2023-03-21 08:00:00', '2023-03-21 18:00:00');
        insert into disponibiliteplateau values ('displat6', 'pt2', '2023-03-22 12:00:00', '2023-03-22 18:00:00');
        insert into disponibiliteplateau values ('displat7', 'pt2', '2023-03-23 08:00:00', '2023-03-23 12:00:00');
        insert into disponibiliteplateau values ('displat8', 'pt2', '2023-03-24 08:00:00', '2023-03-24 08:15:00');

        --add
        insert into disponibiliteplateau values ('displat9', 'pt1', '2023-03-31 06:00:00', '2023-03-31 18:00:00');


        insert into disponibilitepersonnage values ('dispers1', 'p1', '2023-03-22 08:00:00', '2023-03-22 18:00:00');
        insert into disponibilitepersonnage values ('dispers2', 'p2', '2023-03-23 08:00:00', '2023-03-23 12:00:00');

        --add
        insert into disponibilitepersonnage values ('dispers3', 'p3', '2023-03-22 06:00:00', '2023-03-22 07:00:00');
        insert into disponibilitepersonnage values ('dispers4', 'p4', '2023-03-23 08:00:00', '2023-03-23 09:00:00');
        insert into disponibilitepersonnage values ('dispers5', 'p5', '2023-03-24 10:00:00', '2023-03-23 11:00:00');
        --

        insert into heuretravailsemaine values ('htd'||nextval('heuretravailsemaine_seq'),1);
        insert into heuretravailsemaine values ('htd'||nextval('heuretravailsemaine_seq'),7);
        
            --- clean scenario 
            delete from disponibiliteplateau;
            delete from disponibilitepersonnage;
            delete from heuretravailjour;
            delete from heuretravailannee;
            delete from heuretravailsemaine;




