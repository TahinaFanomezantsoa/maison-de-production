
-- database
create database film;

-- connection 
\c film postgres;

create sequence personnage_seq start 1 increment 1; 
create sequence plateau_seq start 1 increment 1;
create sequence personne_seq start 1 increment 1;
create sequence scene_seq start 1 increment 1;
create sequence scenepersonnage_seq start 1 increment 1;
create sequence emotion_seq start 1 increment 1;
create sequence disponibiliteplateau_seq start 1 increment 1;
create sequence disponibilitepersonnage_seq start 1 increment 1;



create table personne (
    id text primary key,
    noms text not null,
    identifiant text unique not null,
    motdepasse text,
    typepersonne text
);

create table personnage 
(
    id text primary key,
    noms text
);

create table emotion 
(
    id text primary key,
    libelle text
);

create table plateau 
(
    id text primary key,
    libelle text,
    description text
);

create table scene 
(
    id text primary key,
    libelle text,
    idpersonne text references personne (id),
    idplateau text references plateau (id),
    tempsdebut time,
    tempsfin time,
    datevalidation timestamp default null,
    datedebutplannification timestamp default null,
    datefinplannification timestamp default null
);

create table scenepersonnage
(
    id text primary key,
    idscene text references scene (id),
    idpersonnage text references personnage (id),
    dialogue text,
    action text,
    idemotion text references emotion (id),
    tempsdebut time,
    tempsfin time
);

create table configurationcontenu (
    nombrecontenuparpage integer
);

create table configurationcontenuplannification (
    nombrecontenuparpage integer
);

create table disponibiliteplateau 
(
    id text primary key,
    idplateau text references plateau (id), 
    datedebut timestamp default null,
    datefin timestamp default null
    
);

create table disponibilitepersonnage
(
    id text primary key,
    idpersonnage text references personnage (id), 
    datedebut timestamp default null,
    datefin timestamp default null 
);

create table jourtravail
(
    id text primary key,
    debut time not null,
    fin time not null
);

ALTER TABLE SCENE 
ADD COLUMN ETAT INTEGER DEFAULT 1;


ALTER TABLE SCENE 
ADD COLUMN ORDRE INTEGER UNIQUE;





--test 
insert into jourtravail values('jt1', '08:00','17:00');

insert into personne values ('pers'||nextval('personne_seq'), 'Rabe Zaka','admin1', 'admin1', 'admin');
insert into personne values ('pers'||nextval('personne_seq'), 'Raboto Be','admin2', 'admin2', 'admin');
insert into personne values ('pers'||nextval('personne_seq'), 'Razaka Manana','admin3', 'admin3', 'admin');
insert into personne values ('pers'||nextval('personne_seq'), 'Jean Claude','auteur1', 'auteur1', 'auteur');
insert into personne values ('pers'||nextval('personne_seq'), 'Gold Ritchler','auteur2', 'auteur2', 'auteur');
insert into personne values ('pers'||nextval('personne_seq'), 'Hank Zipser','auteur3', 'auteur3', 'auteur');

insert into personnage values ('p'||nextval('personnage_seq'),'RAKOTO');
insert into personnage values ('p'||nextval('personnage_seq'),'RABE');
insert into personnage values ('p'||nextval('personnage_seq'),'RASOA');
insert into personnage values ('p'||nextval('personnage_seq'),'RAJAO');
insert into personnage values ('p'||nextval('personnage_seq'),'Jean');
insert into personnage values ('p'||nextval('personnage_seq'),'Jeanne');

insert into personnage values ('p'||nextval('personnage_seq'),'Mandresy');
insert into personnage values ('p'||nextval('personnage_seq'),'Patron');
insert into personnage values ('p'||nextval('personnage_seq'),'Client Mamy');

insert into emotion values ('e'||nextval('emotion_seq'),'joie');
insert into emotion values ('e'||nextval('emotion_seq'),'tristesse');
insert into emotion values ('e'||nextval('emotion_seq'),'colere');
insert into emotion values ('e'||nextval('emotion_seq'),'peur');
insert into emotion values ('e'||nextval('emotion_seq'),'degout');
insert into emotion values ('e'||nextval('emotion_seq'),'surprise');
insert into emotion values ('e'||nextval('emotion_seq'),'honte');
insert into emotion values ('e'||nextval('emotion_seq'),'envie');
insert into emotion values ('e'||nextval('emotion_seq'),'amour');
insert into emotion values ('e'||nextval('emotion_seq'),'culpabilite');


insert into emotion values ('e'||nextval('emotion_seq'),'presse');
insert into emotion values ('e'||nextval('emotion_seq'),'empathie');
insert into emotion values ('e'||nextval('emotion_seq'),'stressee'); 
insert into emotion values ('e'||nextval('emotion_seq'),'fierte'); 

insert into plateau values ('pt'||nextval('plateau_seq'),'Morarano','ao aminy tranony Mandresy');
insert into plateau values ('pt'||nextval('plateau_seq'),'Mandrimena','ao aminy ITU');
insert into plateau values ('pt'||nextval('plateau_seq'),'Mandrimena','ao aminy Galana Tsena');
insert into plateau values ('pt'||nextval('plateau_seq'),'Andoharanofotsy','ao aminy Tsena');
insert into plateau values ('pt'||nextval('plateau_seq'),'Mandrimena','ao aminy Galana Ivelany');

insert into plateau values ('pt'||nextval('plateau_seq'),'Morarano','eo aminy lalana en face ny tranony Mandresy');
insert into plateau values ('pt'||nextval('plateau_seq'),'Anosy','ao ampiasana any Mandresy');
insert into plateau values ('pt'||nextval('plateau_seq'),'Anosy','en face ny fiasanany Mandresy');


insert into scene values ('s'||nextval('scene_seq'),'Mividy Produit','pers4','pt3','08:00','08:15', null, null, null,1,1);
insert into scene values ('s'||nextval('scene_seq'),'Misennuyer le caissiere','pers4','pt3','08:20','08:45', null, null, null,1,2);
insert into scene values ('s'||nextval('scene_seq'),'Mametaka affiche','pers4','pt5','08:50','09:00', null, null, null,1,3);
insert into scene values ('s'||nextval('scene_seq'),'Misy sarina affiche','pers4','pt5','09:05','09:07', null, null, null,1,4);
insert into scene values ('s'||nextval('scene_seq'),'Sarina site web','pers4','pt5','09:07','09:12', null, null, null,1,5);
insert into scene values ('s'||nextval('scene_seq'),'Misy olona mdev','pers4','pt5','09:12','09:20', null, null, null,1,6);
insert into scene values ('s'||nextval('scene_seq'),'Finalite du tsena galana','pers4','pt5','09:20','09:30', null, null, null,1,7);

insert into scene values ('s'||nextval('scene_seq'),'Le Réveil Matinal','pers5','pt1','08:00','08:20', null, null, null,1,8);
insert into scene values ('s'||nextval('scene_seq'),'Acte de Gentillesse','pers5','pt6','08:20','08:30', null, null, null,1,9);
insert into scene values ('s'||nextval('scene_seq'),'Le Début de la Journée de Travail','pers5','pt7','08:30','08:40', null, null, null,1,10);
insert into scene values ('s'||nextval('scene_seq'),'Le Client Difficile','pers5','pt7','08:40','09:10', null, null, null,1,11);
insert into scene values ('s'||nextval('scene_seq'),'La Résolution du Problème','pers5','pt7','09:10','09:30', null, null, null,1,12);
insert into scene values ('s'||nextval('scene_seq'),'La Rencontre Amoureuse','pers5','pt8','09:30','09:45', null, null, null,1,13);
insert into scene values ('s'||nextval('scene_seq'),'La Détente à la Maison','pers5','pt1','09:45','10:00', null, null, null,1,14);



update scene set datevalidation='2023-03-01 23:43:00',datedebutplannification='2023-03-02 08:00:00',datefinplannification='2023-03-02 08:15:00' where id='s1';
update scene set datevalidation='2023-03-01 23:43:00',datedebutplannification='2023-03-02 08:20:00',datefinplannification='2023-03-02 08:45:00' where id='s2';
update scene set datevalidation='2023-03-01 23:43:00',datedebutplannification='2023-03-02 08:50:00',datefinplannification='2023-03-02 09:00:00' where id='s3';
update scene set datevalidation='2023-03-01 23:43:00',datedebutplannification='2023-03-02 09:05:00',datefinplannification='2023-03-02 09:07:00' where id='s4';
update scene set datevalidation='2023-03-01 23:43:00',datedebutplannification='2023-03-02 09:07:00',datefinplannification='2023-03-02 09:12:00' where id='s5';
update scene set datevalidation='2023-03-01 23:43:00',datedebutplannification='2023-03-02 09:12:00',datefinplannification='2023-03-02 09:20:00' where id='s6';
update scene set datevalidation='2023-03-01 23:43:00',datedebutplannification='2023-03-02 08:20:00',datefinplannification='2023-03-02 08:30:00' where id='s7';


insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s1','p1',null,'manolotra produit eo aminy caisse','e1','08:00','08:10');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s1','p1',null,'mivoaka ny super marche','e1','08:10','08:15');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s2',null,null,'mande b ny fotoana nefa tsisy mpividy',null,'08:20','08:35');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s2','p3','pffffffffff','mysennuyer b le caissiere','e2','08:35','08:45');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s3','p2',null,'misy olona iray manao pub mametaka affiche eo aminy galana tsena','e1','08:50','09:00');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s4',null,null,'misy affiche misy momba ana produit iray anilay tsena any galana misy reduction',null,'09:05','09:07');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s5',null,null,'misy description site web any tsena any galana',null,'09:07','09:12');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s6','p5',null,'mdevelloper anle site web any tsena galana',null,'09:12','09:20');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s6',null,null,'Misy client maro be amizay le galana',null,'09:20','09:23');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s7','p6',null,'misy client iray mividy ,io client io dia anisany ilay client milahatra be','e1','09:23','09:27');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s7','p3','Misaotra tompoko !!','faly ilay caissiere mandray anle client','e1','09:27','09:30');

insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s8','p7','Oh non, je suis en retard !','Un homme se réveille dans son lit et regarde lheure sur son téléphone','e13','08:00','08:02');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s8','p7',null,'Il se lève rapidement et se prépare pour la journée en se brossant les dents et en shabillant','e11','08:02','08:20');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s9','p7',null,'Lhomme sort de son appartement et commence à marcher vers le métro','e11','08:20','08:25');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s9','p7','laissez-moi vous aider','En chemin, il croise une vieille dame qui lui demande de laide pour traverser la rue','e12','08:25','08:30');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s10','p7',null,'Lhomme arrive au travail et se met immédiatement au travail','e8','08:30','08:35');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s10','p8','Bonjour, jai besoin que vous vous occupiez dun client difficile','Il est interrompu par son patron qui lui demande de soccuper dun client difficile','e6','08:35','08:40');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s11','p7',null,'Lhomme rencontre le client et essaie de résoudre ses problèmes',null,'08:40','09:00');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s11','p9','Cest inacceptable, je ne veux pas de cette solution !','Le client devient de plus en plus en colère et commence à crier','e3','09:00','09:10');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s12','p7','Je suis heureux de vous avoir aidé à trouver une solution satisfaisante','Lhomme parvient finalement à apaiser le client et lui offre une solution satisfaisant','e14','09:10','09:20');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s12','p9','Je suis desole pour tout a lheure , je vous remercie','Le client sen va, heureux','e14','09:20','09:30');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s13','p7',null,'Lhomme sort du travail et rencontre une femme quil aime bien','e8','09:30','09:35');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s13','p7','Cétait sympa de te revoir, peut-être quon pourrait se voir plus tard cette semaine','Ils se parlent brièvement avant que lhomme ne rentre chez lui','e9','09:35','09:45');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s14','p7','Enfin, je peux me détendre un peu','Lhomme rentre chez lui et se détend en regardant la télévision',null,'09:45','09:50');
insert into scenepersonnage values ('sp'||nextval('scenepersonnage_seq'),'s14','p7',null,'Il sendort sur le canapé',null,'09:45','09:50');

insert into configurationcontenu values (3);

insert into configurationcontenuplannification values (10);

create or replace view sceneplanifie as select * from scene where datevalidation is not null order by datedebutplannification;














