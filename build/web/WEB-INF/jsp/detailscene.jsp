<%@page import="model.Emotion"%>
<%@page import="model.Personnage"%>
<%@page import="model.ScenePersonnage"%>
<%@page import="java.util.List"%>
<%@page import="model.Plateau"%>
<%@page import="model.Personne"%>
<%@page import="model.Scene"%>
<%
    String searchvalue = request.getParameter("searchvalue");
    String id = (String) request.getSession().getAttribute("id");
    Scene scene = (Scene)request.getAttribute("scene");
    Personne personne = (Personne)request.getAttribute("personne");
    Plateau plateau = (Plateau)request.getAttribute("plateau");
    List<ScenePersonnage> listescenepersonnage = (List<ScenePersonnage>)request.getAttribute("listescenepersonnage");
    List<Personnage> listpersonnage = (List<Personnage>)request.getAttribute("listpersonnage");
    List<Emotion> listemotion = (List<Emotion>)request.getAttribute("listemotion");
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />

        <title>Liste</title>
        <meta content="" name="description" />
        <meta content="" name="keywords" />

        <!-- Favicons -->
        <link href="assets/img/favicon.png" rel="icon" />
        <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon" />

        <!-- Google Fonts -->
        <link href="https://fonts.gstatic.com" rel="preconnect" />
        <link
            href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
            rel="stylesheet"
            />

        <!-- Vendor CSS Files -->
        <link
            href="assets/vendor/bootstrap/css/bootstrap.min.css"
            rel="stylesheet"
            />
        <link
            href="assets/vendor/bootstrap-icons/bootstrap-icons.css"
            rel="stylesheet"
            />
        <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet" />
        <link href="assets/vendor/quill/quill.snow.css" rel="stylesheet" />
        <link href="assets/vendor/quill/quill.bubble.css" rel="stylesheet" />
        <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet" />
        <link href="assets/vendor/simple-datatables/style.css" rel="stylesheet" />

        <!-- Template Main CSS File -->
        <link href="assets/css/style.css" rel="stylesheet" />
    </head>

    <style>
        @media screen and (min-width: 676px) {
            .big {
                max-width: 35%; /* New width for default modal */
            }
            .small {
                max-width: 20%; /* New width for default modal */
            }
        }
        .scrollable {
            max-height: calc(100vh - 200px);
            overflow-y: auto;
        }
        a.logout:hover {
            color: rgba(255, 0, 0, 0.596);
        }
        a.login:hover {
            color: #9e9ae4;
        }
    </style>

    <body>

        <div class="modal fade" id="nouveauScene" role="dialog" tabindex="-1">
            <div class="modal-dialog small" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Connectez �votre compte</h5>
                        <button
                            type="button"
                            class="btn-close"
                            data-bs-dismiss="modal"
                            aria-label="Close"
                            ></button>
                    </div>
                    <div class="modal-body">
                        <form class="row g-3 needs-validation" novalidate method="post" action="login">
                            <div class="col-12">
                                <label for="identifiant" class="form-label">Identifiant</label>
                                <div class="input-group has-validation">
                                    <input
                                        type="text"
                                        name="identifiant"
                                        class="form-control"
                                        id="identifiant"
                                        required=""
                                        autocomplete="off"
                                        />
                                    <div class="invalid-feedback">
                                        Entrer une identifiant valide!
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <label for="motdepasse" class="form-label">Mot de passe</label>
                                <input
                                    type="password"
                                    name="motdepasse"
                                    class="form-control"
                                    id="motdepasse"
                                    required=""
                                    autocomplete="off"
                                    />
                                <div class="invalid-feedback">
                                    Entrer un mot de passe valide!
                                </div>
                            </div>

                            <div class="col-12">
                                <button class="btn btn-primary" type="submit">
                                    Se connecter
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
                <!-- ======= Header ======= -->
        <header id="header" class="header fixed-top d-flex align-items-center">
            <div class="d-flex align-items-center justify-content-between">
                <a class="logo d-flex align-items-center">
                    <i class="bi bi-film"></i>
                    <span class="d-none d-lg-block">Maison de Prod</span>
                </a>
                <i class="bi bi-list toggle-sidebar-btn"></i>
            </div>
            <!-- End Logo -->

                        <div class="search-bar">
                <form
                    class="search-form d-flex align-items-center"
                    method="GET"
                    action=""
                    >

                    <input
                        type="text"
                        name="searchvalue"
                        <% if (searchvalue
                                    != null && !searchvalue.isEmpty()) {
                                out.print("value=\"" + searchvalue + "\"");
                            } %>
                        placeholder="Entrer votre recherche"
                        title="Entrer votre recherche"
                        />
                    <button type="submit" title="Search">
                        <i class="bi bi-search"></i>
                    </button>
                </form>
            </div>
            <!-- End Search Bar -->
            
            <nav class="header-nav ms-auto">
                <ul class="d-flex align-items-center">
                    <li class="nav-item d-block d-lg-none">
                        <a class="nav-link nav-icon search-bar-toggle" href="#">
                            <i class="bi bi-search"></i>
                        </a>
                    </li>
                    <!-- End Search Icon-->

                    <% if (id
                                != null && !id.equals(
                                        "null")) {%>
                    <li class="nav-item dropdown pe-3">

                        <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
                            <!--<img src="assets/img/profile-img.jpg" alt="Profile" class="rounded-circle">-->
                            <span class="d-none d-md-block dropdown-toggle ps-2"><%= request.getSession().getAttribute("noms")%></span>
                        </a><!-- End Profile Iamge Icon -->

                        <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                            <li class="dropdown-header">
                                <h6><%= request.getSession().getAttribute("noms")%></h6>
                                <span><%= request.getSession().getAttribute("typepersonne")%></span>
                            </li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li class="dropdown-header">
                                <a class="dropdown-item d-flex align-items-center" href="logout">
                                    <i class="bi bi-box-arrow-right"></i>
                                    <span>Se d�connecter</span>
                                </a>
                            </li>

                        </ul><!-- End Profile Dropdown Items -->
                    </li><!-- End Profile Nav -->
                    <%}%>


                    <li>
                        <a class="nav-link nav-profile d-flex align-items-center pe-5 login" data-bs-toggle="modal" data-bs-target="#nouveauScene" href="#"  >
                            <!-- Se connecter si non connecter and use modal si connecter Se deconnecter and use link-->

                            <%if (id
                                        == null || id.equals(
                                                "null")) {%>
                            <span>Se connecter</span> 
                            <%}%>
                        </a>
                    </li>
                    <!-- End Profile Nav -->
                </ul>
            </nav>
            <!-- End Icons Navigation -->
        </header>
        <!-- End Header -->
        <!-- ======= Sidebar ======= -->
        <aside id="sidebar" class="sidebar">
            <ul class="sidebar-nav" id="sidebar-nav">
                <li class="nav-item">
                    <a
                        class="nav-link collapsed"
                        data-bs-target="#components-nav"
                        data-bs-toggle="collapse"
                        href="#"
                        >
                        <i class="bi bi-grid"></i></i><span>Sc�ne</span
                        ><i class="bi bi-chevron-down ms-auto"></i>
                    </a>
                    <ul
                        id="components-nav"
                        class="nav-content collapse"
                        data-bs-parent="#sidebar-nav"
                        >
                        <% if (id
                                    != null && !id.equals(
                                            "null")) { %>
                        <li>
                            <a href="nouveauscene">
                                <i class="bi bi-circle"></i><span>Nouveau</span>
                            </a>
                        </li>
                        <%}%>
                        <li>
                            <a href="listescene">
                                <i class="bi bi-circle"></i><span>Liste</span>
                            </a>
                        </li>
                         <li>
                            <!--LIEN DE PLANNIFICATION-->
                            <a href="plannification">
                                <i class="bi bi-circle"></i><span>Plannification</span>
                            </a>
                        </li>
                        
                    </ul>
                </li>

                <!-- End Dashboard Nav -->


            </ul>
        </aside>
        <!-- End Sidebar-->
        
        <main id="main" class="main">

            <%if (request.getParameter(
                        "loginerr") != null) {%>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <i class="bi bi-exclamation-octagon me-1"></i>
                Erreur de connexion !
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>

            <%}%>
            <%if (request.getParameter(
                        "loginsucc") != null) {%>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <i class="bi bi-check-circle me-1"></i>
                Vous �tes connect� avec succ�s !
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            <%}%>

            <%if (request.getParameter(
                        "logoutsucc") != null) {%>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <i class="bi bi-check-circle me-1"></i>
                Vous �tes d�connect� avec succ�s !
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            <%}%>
            <%if (request.getParameter(
                        "modifsucc") != null) {%>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <i class="bi bi-check-circle me-1"></i>
                Contenu modifi� avec succ�s !
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>

            <%}%>

            <%if (request.getParameter("modifcontenuparpagesucc") != null) {%>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <i class="bi bi-check-circle me-1"></i>
                Nombre de contenu par page modifi� avec succ�s !
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            <%}%>

            <div class="pagetitle">
                <h1>
                    Sc�ne : <%= scene.getLibelle() %> 
                </h1>
                    <p style="margin-top: 2px;" > <strong>Plateau : </strong><%= plateau.getLibelle()+" "+plateau.getDescription() %> </p>
            </div>
            
            
            <div class="card">
          <div class="card-body">
            <h5 class="card-title">Action</h5>

            <!-- Default Table -->
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Action</th>
                  <th scope="col">Personnage</th>
                  <th scope="col">Emotion</th>
                  <th scope="col">Dialogue</th>
                  <th scope="col">tempsdebut</th>
                  <th scope="col">tempsfin</th>
                </tr>
              </thead>
              <tbody>
                <% for (int i = 0 ; i< listescenepersonnage.size() ; i++ ) { %>
                  <tr>
                  <td><%= listescenepersonnage.get(i).getAction()  %></td>
                  <td><%= (listpersonnage.get(i).getNoms() != null)?listpersonnage.get(i).getNoms():"pas de personnage" %></td>
                  <td><%= (listemotion.get(i).getLibelle() != null)?listemotion.get(i).getLibelle():"pas d'emotion" %></td>
                  <td><%= (listescenepersonnage.get(i).getDialogue() != null)?listescenepersonnage.get(i).getDialogue():"pas de dialogue" %></td>
                  <td><%= listescenepersonnage.get(i).getTempsdebut()%></td>
                  <td><%= listescenepersonnage.get(i).getTempsfin()%></td>
                </tr>
                <% } %>
              </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>
            
            
        </main>
        <!-- End #main -->
        
        <!-- ======= Footer ======= -->
        <footer id="footer" class="footer">
            <div class="copyright">
                &copy; Copyright <strong><span>CMS Company</span></strong
                >. All Rights Reserved
            </div>
            <div class="credits">
                Designed by <a href="#">Yels</a>
            </div>
        </footer>
        <!-- End Footer -->

        <a
            href="#"
            class="back-to-top d-flex align-items-center justify-content-center"
            ><i class="bi bi-arrow-up-short"></i
            ></a>

        <!-- Vendor JS Files -->
        <script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/vendor/chart.js/chart.umd.js"></script>
        <script src="assets/vendor/echarts/echarts.min.js"></script>
        <script src="assets/vendor/quill/quill.min.js"></script>
        <script src="assets/vendor/simple-datatables/simple-datatables.js"></script>
        <script src="assets/vendor/tinymce/tinymce.min.js"></script>
        <script src="assets/vendor/php-email-form/validate.js"></script>

        <!-- Template Main JS File -->
        <script src="assets/js/main.js"></script>
        <script src="assets/js/jquery.js"></script>
        <script>


                                    var alertEl = document.querySelector('.alert');
                                    setTimeout(function () {
                                        if (alertEl !== null) {
                                            alertEl.classList.remove('show');
                                            alertEl.classList.add('d-none');
                                        }
                                    }, 1000);

                                    $(document).on("click", ".modifclass", function () {
                                        $('#preview').attr('src', $(this).data('imagedata'));
                                        $(".modal-body #libelle").val($(this).data('libelle'));
                                        $(".modal-body #id").val($(this).data('id'));
                                        $(".modal-body #page").val($(this).data('page'));
                                        $(".modal-body #idplateau").val($(this).data('idplateau'));

                                        $(".modal-body #datedebut").val($(this).data('datedebut'));
                                        $(".modal-body #datefin").val($(this).data('datefin'));




                                        var searchvalue = $(this).data('searchvalue');
                                        if (searchvalue != null) {
                                            $(".modal-body #searchvalue").val(searchvalue);
                                        }
                                    });
                                    function submitForm() {
                                        document.getElementById("myForm").submit();
                                    }

        </script>
  </body>