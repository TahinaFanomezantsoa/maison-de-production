/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelUtils;

import lombok.Getter;
import lombok.Setter;
import util.Column;

/**
 *
 * @author charles
 */
@Getter
@Setter
public class BaseModel {

    @Column(name = "id")
    private String id;

}
