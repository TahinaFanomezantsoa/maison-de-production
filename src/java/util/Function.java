/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import model.Intervalle;

/**
 *
 * @author elsys
 */
public class Function {

    public static String convertTimestampToDate(Timestamp timestamp) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE dd'th' MMMM yyyy HH:mm"); // Define desired date format       
        return dateFormat.format(timestamp); // Format Date object as string and return
    }

    public static String getDate(Timestamp timestamp) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE dd MMMM yyyy"); // Define desired date format     
            return dateFormat.format(timestamp); // Format Date object as string and return

        } catch (Exception e) {
            return null;
        }

    }

    public static Time getTimeInTimestamp(Timestamp timestamp) {
        return new Time(timestamp.getTime());
    }

    public static Instant TimeToInstant(Time time) {
        String timeString = time.toString(); // Example time string
        LocalTime localTime = LocalTime.parse(timeString); // Parse the time string
        LocalDate localDate = LocalDate.now(); // Today's date
        ZoneId zoneId = ZoneId.systemDefault(); // Current time zone

        Instant instant = localTime.atDate(localDate).atZone(zoneId).toInstant(); // Convert to Instant
        return instant;
    }

    public static boolean intervalOfTimestampIsInIntervalOfTimestamp(Time debut1, Time fin1, Time debut2, Time fin2) {

        Instant start1 = TimeToInstant(debut1);
        Instant end1 = TimeToInstant(fin1);

        Instant start2 = TimeToInstant(debut2);
        Instant end2 = TimeToInstant(fin2);

        Duration duration1 = Duration.between(start1, end1);
        Duration duration2 = Duration.between(start2, end2);

        if (duration1.compareTo(duration2) < 0) {
            return false;
        }
        return start2.isAfter(start1) || start2.equals(start1) && end2.isBefore(end1) || end2.equals(end1);

    }

    public static String getTime(Timestamp timestamp) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm"); // Define desired date format       
            return dateFormat.format(timestamp); // Format Date object as string and return

        } catch (Exception e) {
            return null;
        }

    }

    // check if a column exist in resultset 
    public static boolean isExist(ResultSet rs, String column) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            if (metaData.getColumnName(i).equalsIgnoreCase(column)) {
                return true;
            }
        }
        return false;
    }

    // get Field by name
    public static Field getField(Object object, String fieldName) {
        Field[] fields = getAllFields(object);
        for (Field field : fields) {
            if (field.getName().equals(fieldName)) {
                return field;
            }
        }
        return null;
    }

// set Object value
    public static void setObjectValue(Object object, Field field, Object value) throws Exception {
        object.getClass().getMethod("set" + field.getName().substring(0, 1).toUpperCase().concat(field.getName().substring(1)), field.getType()).invoke(object, value);
    }

// set Object value
    public static Object getObjectValue(Object object, Field field) throws Exception {
        System.out.println(object.getClass().getMethod("get" + field.getName().substring(0, 1).toUpperCase().concat(field.getName().substring(1))));
        return object.getClass().getMethod("get" + field.getName().substring(0, 1).toUpperCase().concat(field.getName().substring(1))).invoke(object);
    }

// get annoted table name 
    public static String getTableName(Object object) {
        return object.getClass().getAnnotation(Table.class).name();
    }
// get annoted Column 

    public static Column getAttributeName(Field field) {
        if (field.getAnnotation(Column.class) != null) {
            return field.getAnnotation(Column.class);
        }
        return null;
    }
// get this Fields

    public static Field[] getThisFields(Object object) {
        return object.getClass().getDeclaredFields();
    }

// get super Fields
    public static Field[] getSuperFields(Object object) {
        return object.getClass().getSuperclass().getDeclaredFields();
    }

// get all Fields
    public static Field[] getAllFields(Object object) {
        Field[] superFields = getSuperFields(object);
        Field[] thisFields = getThisFields(object);

// concate table
        int len1 = superFields.length;
        int len2 = thisFields.length;
        Field[] result = new Field[len1 + len2];
        System.arraycopy(superFields, 0, result, 0, len1);
        System.arraycopy(thisFields, 0, result, len1, len2);

        return result;
    }

// get field value in a object
    public static Object getAttributeValue(Object object, Field field) throws Exception {
        String fieldName = field.getName();
        fieldName = fieldName.substring(0, 1).toUpperCase().concat(fieldName.substring(1));
        Object result = null;
        try {
            result = object.getClass().getMethod("get" + fieldName).invoke(object);
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    // get all annoted field 
    public static List<Column> getAttributesName(Object object) {
        List<Column> result = new ArrayList<>();
        for (Field declaredField : getAllFields(object)) {
            if (getAttributeName(declaredField) != null) {
                result.add(getAttributeName(declaredField));
            }
        }
        return result;
    }

    // get all annoted field value 
    public static List getAttributesValue(Object object) throws Exception {
        List result = new ArrayList<>();
        for (Field declaredField : getAllFields(object)) {
            if (getAttributeName(declaredField) != null) {

                try {
                    result.add(getAttributeValue(object, declaredField));
                } catch (Exception e) {
                    throw e;
                }
            }
        }
        return result;
    }

    public static void addSecond(Timestamp timestamp, double secondToAdd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(timestamp);
        cal.add(Calendar.SECOND, (int) secondToAdd);
        timestamp.setTime(cal.getTime().getTime());
    }
    
    public static void addTime (Timestamp timestamp,int day,int month,int year,int hour,int minute,int second) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(timestamp);
        cal.add(Calendar.DATE,day);
        cal.add(Calendar.MONTH, month);
        cal.add(Calendar.YEAR,year);
        cal.add(Calendar.HOUR, hour);
        cal.add(Calendar.MINUTE, minute);
        cal.add(Calendar.SECOND,second);
        timestamp.setTime(cal.getTimeInMillis());
    }
    
}
