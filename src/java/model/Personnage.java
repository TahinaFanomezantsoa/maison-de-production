/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.GenericDao;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import modelUtils.BaseModel;
import util.Column;
import util.Function;
import util.Table;

/**
 *
 * @author charles
 */
@Getter
@Setter
@Table(name = "personnage")
public class Personnage extends BaseModel {

    @Column(name = "noms")
    String noms;

//    public List<Intervalle> getPersonnageDisponible(GenericDao genericdao, Timestamp debut, Timestamp fin) throws Exception {
//        List<Intervalle> result = new Vector<>();
//        DisponibilitePersonnage dp = new DisponibilitePersonnage();
//        dp.setIdpersonnage(getId());
//        List<DisponibilitePersonnage> listnondisponible = (List<DisponibilitePersonnage>) genericdao.find(dp, null, null, null, null, null);
//        Timestamp current = debut;
//        for (DisponibilitePersonnage disponibilite : listnondisponible) {
//            if (current.after(fin)) {
//                break;
//            }
//            if (current.before(disponibilite.getDatedebut())) {
//                Intervalle i = new Intervalle();
//                i.setDebut(new Timestamp(current.getTime()));
//                i.setFin(new Timestamp(disponibilite.getDatedebut().getTime()));
//                result.add(i);
//            }
//            current = disponibilite.getDatefin();
//        }
//        if (current.before(fin)) {
//            Intervalle i = new Intervalle();
//            i.setDebut(new Timestamp(current.getTime()));
//            i.setFin(new Timestamp(fin.getTime()));
//            result.add(i);
//        }
//        return result;
//    }
    
    public List<Intervalle> getPersonnageDisponible(GenericDao genericdao, Timestamp debut, Timestamp fin) throws Exception {        
        List<Intervalle> resultat = new Vector<>();
        DisponibilitePersonnage dp = new DisponibilitePersonnage();
        dp.setIdpersonnage(getId());

        List<DisponibilitePersonnage> disponnibilite = (List<DisponibilitePersonnage>) genericdao.find(dp, null, null, null, null, null);

        List<HeureTravailJour> heuretravailjour = (List<HeureTravailJour>) genericdao.find(new HeureTravailJour(), null, null, null, null, null);

        List<HeureTravailSemaine> heuretravailsemaine = (List<HeureTravailSemaine>) genericdao.find(new HeureTravailSemaine(), null, null, null, null, null);

        List<HeureTravailAnnee> heuretravailannee = (List<HeureTravailAnnee>) genericdao.find(new HeureTravailAnnee(), null, null, null, null, null);

        Time timefin = Time.valueOf(fin.toLocalDateTime().toLocalTime());
        Timestamp current = new Timestamp(debut.getTime());
        Calendar calcurrent = Calendar.getInstance();
        while (!(fin.before(current) || fin.equals(current))) {
            calcurrent.setTime(current);
            if (!(heuretravailsemaine.stream().anyMatch(hs -> hs.getJoursemaine() == calcurrent.get(Calendar.DAY_OF_WEEK)) || heuretravailannee.stream().anyMatch(ha -> {
                Calendar c = Calendar.getInstance();
                c.setTime(ha.getDate());
                return c.get(Calendar.DATE) == calcurrent.get(Calendar.DATE) && c.get(Calendar.MONTH) == calcurrent.get(Calendar.MONDAY);
            }))) {
                List<HeureTravailJour> htjour = new Vector<HeureTravailJour>();
                htjour.addAll(heuretravailjour);
                for (DisponibilitePersonnage disponibilitePersonnage : disponnibilite) {
                    if (current.toLocalDateTime().toLocalDate().equals(disponibilitePersonnage.getDatedebut().toLocalDateTime().toLocalDate())) {
                        HeureTravailJour hj = new HeureTravailJour();
                        hj.setDebut(Time.valueOf(disponibilitePersonnage.getDatedebut().toLocalDateTime().toLocalTime()));
                        hj.setFin(Time.valueOf(disponibilitePersonnage.getDatefin().toLocalDateTime().toLocalTime()));
                        htjour.add(hj);
                    }
                }
                Collections.sort(htjour, Comparator.comparing(HeureTravailJour::getDebut));
                
                Date currentdate = Date.valueOf(current.toLocalDateTime().toLocalDate());
                Time currenttimed = Time.valueOf(current.toLocalDateTime().toLocalTime());
                Time currenttimef = null;
                
                for (HeureTravailJour heureTravailJour : htjour) {
                    if (currenttimed.after(heureTravailJour.getDebut())) {
                        if (currenttimed.before(heureTravailJour.getFin())) {
                            currenttimed = new Time(heureTravailJour.getFin().getTime());
                        }
                    }
                    if (currenttimed.before(heureTravailJour.getDebut())) {
                        currenttimef = new Time(heureTravailJour.getDebut().getTime());
                    }
                    if (currenttimed.equals(heureTravailJour.getDebut())) {
                        currenttimed = new Time(heureTravailJour.getFin().getTime());
                    }
                    if (currenttimef != null) {
                        if (currentdate.compareTo(Date.valueOf(fin.toLocalDateTime().toLocalDate())) == 0  ) {
                            if (timefin.before(currenttimed)) {
                                break;
                            }
                            if (timefin.before(currenttimef)) {
                                currenttimef = Time.valueOf(fin.toLocalDateTime().toLocalTime());
                            }
                        }
                        Intervalle i = new Intervalle();
                        i.setDebut(Timestamp.valueOf(LocalDateTime.of(currentdate.toLocalDate(),currenttimed.toLocalTime())));
                        i.setFin(Timestamp.valueOf(LocalDateTime.of(currentdate.toLocalDate(),currenttimef.toLocalTime())));
                        resultat.add(i);
                        currenttimed = new Time(heureTravailJour.getFin().getTime()) ;
                        currenttimef = null;
                    }
                }
//                if (currenttimed.after(Time.valueOf("00:00:00"))) {
//                   Intervalle i = new Intervalle();
//                   i.setDebut(Timestamp.valueOf(LocalDateTime.of(currentdate.toLocalDate(),currenttimed.toLocalTime())));
//                   i.setFin(Timestamp.valueOf(LocalDateTime.of(currentdate.toLocalDate(),Time.valueOf("00:00:00").toLocalTime())));
//                   Function.addTime(i.getFin(), 1, 0, 0, 0, 0, 0);
//                   resultat.add(i);
//                }
            }
            Function.addTime(current, 1, 0, 0, 0, 0, 0);
            current = Timestamp.valueOf(LocalDateTime.of(current.toLocalDateTime().toLocalDate(),Time.valueOf("00:00:00").toLocalTime()));
        }

        return resultat;
    }
    
}
