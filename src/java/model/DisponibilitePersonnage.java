/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;
import lombok.Getter;
import lombok.Setter;
import modelUtils.BaseModel;
import util.Column;
import util.Table;

/**
 *
 * @author charles
 */
@Getter
@Setter
@Table(name = "disponibilitepersonnage")
public class DisponibilitePersonnage extends BaseModel {

  
    @Column(name = "idpersonnage")
    String idpersonnage;
    
    @Column(name = "datedebut")
    Timestamp datedebut;
    
    @Column(name = "datefin")
    Timestamp datefin;

}
