/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Time;
import lombok.Getter;
import lombok.Setter;
import modelUtils.BaseModel;
import util.Column;
import util.Table;

/**
 *
 * @author Murphy
 */

@Getter
@Setter
@Table(name = "heuretravailjour")
public class HeureTravailJour extends BaseModel {
    
    @Column(name = "debut")
    Time debut;
    @Column(name = "fin")
    Time fin;

    @Override
    public String toString() {
        return "HeureTravailJour{" + "debut=" + debut + ", fin=" + fin + '}';
    }
     
}
