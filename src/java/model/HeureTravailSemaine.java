/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.Getter;
import lombok.Setter;
import modelUtils.BaseModel;
import util.Column;
import util.Table;

/**
 *
 * @author Murphy
 */

@Getter
@Setter
@Table(name = "heuretravailsemaine")
public class HeureTravailSemaine extends BaseModel {
    
    @Column(name = "joursemaine")
    Integer joursemaine;
    
}
