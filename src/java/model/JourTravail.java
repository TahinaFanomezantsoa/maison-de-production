/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.GenericDao;
import java.sql.Time;
import lombok.Getter;
import lombok.Setter;
import modelUtils.BaseModel;
import util.Column;
import util.Table;

/**
 *
 * @author itu
 */
@Getter
@Setter
@Table(name = "jourtravail")
public class JourTravail extends BaseModel{
    @Column(name = "debut")
    Time debutJournee;
    @Column(name = "fin")
    Time finJournee;
}
