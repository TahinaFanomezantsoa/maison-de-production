/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Murphy
 */

@Setter
@Getter
public class Intervalle {
    
    Timestamp debut;
    Timestamp fin;

    @Override
    public String toString() {
        return "Intervalle{" + "debut=" + debut + ", fin=" + fin + '}';
    }
    
    
    
}
