/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.GenericDao;
import java.sql.Timestamp;
import java.util.List;
import java.util.Vector;
import lombok.Getter;
import lombok.Setter;
import modelUtils.BaseModel;
import util.Column;
import util.Table;

/**
 *
 * @author charles
 */
@Getter
@Setter
@Table(name = "disponibiliteplateau")
public class DisponibilitePlateau extends BaseModel {

    
    @Column(name = "idplateau")
    String idplateau;
    
    @Column(name = "datedebut")
    Timestamp datedebut;
    
    @Column(name = "datefin")
    Timestamp datefin;

}
