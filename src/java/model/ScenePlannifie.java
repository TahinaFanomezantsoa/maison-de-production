/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.GenericDao;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import lombok.Setter;
import modelUtils.BaseModel;
import util.Column;
import util.Table;

/**
 *
 * @author itu
 */
@Getter
@Setter
@Table(name = "sceneplanifie")
public class ScenePlannifie extends BaseModel{
    
    @Column(name = "idpersonne")
    String idpersonne;

    @Column(name = "libelle")
    String libelle;

    @Column(name = "idplateau")
    String idplateau;

    @Column(name = "tempsdebut")
    Time tempsdebut;

    @Column(name = "tempsfin")
    Time tempsfin;
    
    @Column(name = "datevalidation")
    Timestamp datevalidation;
    
     @Column(name = "datedebutplannification")
    Timestamp datedebutplannification;
     
      @Column(name = "datefinplannification")
    Timestamp datefinplannification;
    
     public void setTempsAll(String tempsdebut, String tempsfin){
         if (tempsdebut != null && !tempsdebut.isEmpty()) {
            setTempsdebut(Time.valueOf(tempsdebut));
        }
        if (tempsfin != null && !tempsfin.isEmpty()) {
            setTempsfin(Time.valueOf(tempsfin));
        }
    }
     
      public void setDatePlannification(String datedebutplannification, String datefinplannification) {
        if (datedebutplannification != null && !datedebutplannification.isEmpty()) {
            setDatedebutplannification(Timestamp.valueOf(datedebutplannification.replace("T", " ") + ":00"));
        }
        if (datefinplannification != null && !datefinplannification.isEmpty()) {
            setDatefinplannification(Timestamp.valueOf(datefinplannification.replace("T", " ") + ":00"));
        }

    }

    public Plateau getPlateau(GenericDao genericDao) throws Exception {
        Plateau plateau = new Plateau();
        plateau.setId(idplateau);
        return (Plateau) genericDao.findById(plateau);
    }

    public static int getNombrecontenuparpage(GenericDao genericDao) throws Exception {
        return (int) genericDao.executeQuery("select nombrecontenuparpage from configurationcontenuplannification", new String[]{"nombrecontenuparpage"}).get(0).get("nombrecontenuparpage");
    }

    public static List<Integer> getNombrepagination(GenericDao genericDao, String query) throws Exception {
        List<Integer> list = new ArrayList<>();
        int nombrecontenuparpage = getNombrecontenuparpage(genericDao);
        long count = genericDao.countRow(query);
        double nombre = (double) count / (double) nombrecontenuparpage;

        if (nombre == 0) {
            return list;
        }
        for (int i = 1; i < nombre + 1; i++) {
            list.add(i);
        }
        return list;

    }

    public String getDateString() {
        return "<b>Date : </b> du " + tempsdebut + " au " + tempsfin + "<br>";
    }
}

