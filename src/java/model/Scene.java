/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.GenericDao;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import lombok.Setter;
import modelUtils.BaseModel;
import util.Column;
import util.Function;
import util.Table;

/**
 *
 * @author charles
 */
@Getter
@Setter
@Table(name = "scene")
public class Scene extends BaseModel {

    @Column(name = "idpersonne")
    String idpersonne;

    @Column(name = "libelle")
    String libelle;

    @Column(name = "idplateau")
    String idplateau;

    @Column(name = "tempsdebut")
    Time tempsdebut;

    @Column(name = "tempsfin")
    Time tempsfin;

    @Column(name = "datevalidation")
    Timestamp datevalidation;

    @Column(name = "datedebutplannification")
    Timestamp datedebutplannification;

    @Column(name = "datefinplannification")
    Timestamp datefinplannification;

    @Column(name = "etat")
    Integer etat;

    @Column(name = "ordre")
    Integer ordre;

    String details = "";

    public void setDetailsPerso(GenericDao gd) throws Exception {

        ScenePersonnage sp = new ScenePersonnage();
        sp.setIdscene(this.getId());

        List<ScenePersonnage> lsp = gd.find(sp, null, null, null, null, null);

        for (ScenePersonnage scenePersonnage : lsp) {
            Personnage p = new Personnage();
            p.setId(scenePersonnage.getIdpersonnage());
            p = (Personnage) gd.findById(p);

            Emotion e = new Emotion();
            e.setId(scenePersonnage.getIdemotion());
            e = (Emotion) gd.findById(e);

            details = details + ("Personnage : " + p.getNoms() + "-" + "Dialogue : " + scenePersonnage.getDialogue() + "-" + "Action :" + scenePersonnage.getAction() + "-" + "Emotion : " + e.getLibelle() + "-" + scenePersonnage.getTempsPerso() + "|");
        }

    }

    public void setTempsAll(String tempsdebut, String tempsfin) {
        if (tempsdebut != null && !tempsdebut.isEmpty()) {
            setTempsdebut(Time.valueOf(tempsdebut + ":00"));
        }
        if (tempsfin != null && !tempsfin.isEmpty()) {
            setTempsfin(Time.valueOf(tempsfin + ":00"));
        }
    }

    public void setDatePlannification(String datedebutplannification, String datefinplannification) {
        if (datedebutplannification != null && !datedebutplannification.isEmpty()) {
            setDatedebutplannification(Timestamp.valueOf(datedebutplannification.replace("T", " ") + ":00"));
        }
        if (datefinplannification != null && !datefinplannification.isEmpty()) {
            setDatefinplannification(Timestamp.valueOf(datefinplannification.replace("T", " ") + ":00"));
        }

    }

    public static void updateContenuparpage(int nombrecontenuparpage, GenericDao genericDao) throws Exception {
        genericDao.executeQueryNotReturned("update configurationcontenu set nombrecontenuparpage='" + nombrecontenuparpage + "'");
    }

    public Plateau getPlateau(GenericDao genericDao) throws Exception {
        Plateau plateau = new Plateau();
        plateau.setId(idplateau);
        return (Plateau) genericDao.findById(plateau);
    }

    public static int getNombrecontenuparpage(GenericDao genericDao) throws Exception {
        return (int) genericDao.executeQuery("select nombrecontenuparpage from configurationcontenu", new String[]{"nombrecontenuparpage"}).get(0).get("nombrecontenuparpage");
    }

    public static List<Integer> getNombrepagination(GenericDao genericDao, String query) throws Exception {
        List<Integer> list = new ArrayList<>();
        int nombrecontenuparpage = getNombrecontenuparpage(genericDao);
        long count = genericDao.countRow(query);
        double nombre = (double) count / (double) nombrecontenuparpage;

        if (nombre == 0) {
            return list;
        }
        for (int i = 1; i < nombre + 1; i++) {
            list.add(i);
        }
        return list;
    }

    public String getDateString() {
        return "<b>Heure : </b> du " + tempsdebut + " au " + tempsfin + "<br>";
    }

    //Fonction Maka ny Liste à Planifier
    public static List<Scene> getListePlannifier(GenericDao dao) {
        List<Scene> liste = new Vector<Scene>();
        //List<Object> objet = new Vector<Object>();
        try {
            liste = dao.executeQuery(new Scene(), "select * from scene where datevalidation is not null order by datedebutplannification");
        } catch (Exception ex) {
            Logger.getLogger(Scene.class.getName()).log(Level.SEVERE, null, ex);
        }
//        for(Object o : objet){
//            liste.add((Scene) o);
//        }
        return liste;
    }

    public double getDuree() {

        long millis1 = tempsdebut.getTime();
        long millis2 = tempsfin.getTime();

        if (millis2 < millis1) {
            millis2 += 24 * 60 * 60 * 1000;
        }

        long diffInMillis = millis2 - millis1;

        return (diffInMillis / 1000);
    }

    public static List<Scene> getSceneDisponibleOrdered(Timestamp debut, Timestamp fin, GenericDao genericDao, List<Scene> listescenes) throws Exception {
        Timestamp debutTemp = Timestamp.valueOf(debut.toLocalDateTime());
        List<Scene> resultat = new ArrayList<>();

        for (Scene scene : listescenes) {
            System.out.println("debut temp : " + debutTemp + " --- fin : " + fin);
            Plateau plateau = scene.getPlateau(genericDao);
            List<Intervalle> intervalle = plateau.getPlateauDisponible(genericDao, debutTemp, fin);
            System.out.println(plateau.getId() + "-" + intervalle);
            if (!intervalle.isEmpty()) {
                ScenePersonnage sp = new ScenePersonnage();
                sp.setIdscene(scene.getId());
                if (sp.isDisponible(genericDao, debutTemp, fin)) {

                    // Intervalle intervalle1 = intervalle.get(0);
                    List<Intervalle> listInter = new ArrayList<>();
                    listInter.add(intervalle.get(0));
                    if (sp.getDisponible(genericDao, debutTemp, fin) != null) {
                        listInter.add(sp.getDisponible(genericDao, debutTemp, fin));
                    }
                    Collections.sort(listInter, Comparator.comparing(Intervalle::getDebut).reversed());
                    Intervalle intervalle1 = listInter.get(0);

                    scene.setDatedebutplannification(intervalle1.getDebut());
                    Timestamp finTemp = Timestamp.valueOf(intervalle1.getDebut().toLocalDateTime());

                    Function.addSecond(finTemp, scene.getDuree());

                    scene.setDatefinplannification(finTemp);
                    scene.setDetailsPerso(genericDao);
                    resultat.add(scene);

                    debutTemp = Timestamp.valueOf(intervalle1.getDebut().toLocalDateTime());
                    Function.addSecond(debutTemp, scene.getDuree());
                }

            }

        }

        return resultat;
    }

    @Override
    public String toString() {
        return "Scene{" + "idpersonne=" + idpersonne + ", libelle=" + libelle + ", idplateau=" + idplateau + ", tempsdebut=" + tempsdebut + ", tempsfin=" + tempsfin + ", datevalidation=" + datevalidation + ", datedebutplannification=" + datedebutplannification + ", datefinplannification=" + datefinplannification + ", etat=" + etat + '}';
    }

}
