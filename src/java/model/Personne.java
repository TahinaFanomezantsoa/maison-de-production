/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.Getter;
import lombok.Setter;
import modelUtils.BaseModel;
import util.Column;
import util.Table;
/**
 *
 * @author charles
 */
@Getter
@Setter
@Table(name = "personne")
public class Personne extends BaseModel {
    
    @Column(name = "identifiant")
    String identifiant;

    @Column(name = "noms")
    String noms;

    @Column(name = "motdepasse")
    String motdepasse;

    @Column(name = "typepersonne")
    String typepersonne;

    public String getTypepersonneString() {
        return typepersonne.substring(0, 1).toUpperCase().concat(typepersonne.substring(1));
    }
}
