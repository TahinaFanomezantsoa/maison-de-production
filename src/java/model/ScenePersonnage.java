/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.GenericDao;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import modelUtils.BaseModel;
import util.Column;
import util.Table;

/**
 *
 * @author charles
 */
@Getter
@Setter
@Table(name = "scenepersonnage")
public class ScenePersonnage extends BaseModel {

    @Column(name = "idpersonnage")
    String idpersonnage;

    @Column(name = "idscene")
    String idscene;

    @Column(name = "dialogue")
    String dialogue;

    @Column(name = "action")
    String action;

    @Column(name = "idemotion")
    String idemotion;

    @Column(name = "tempsdebut")
    Time tempsdebut;

    @Column(name = "tempsfin")
    Time tempsfin;

    public void setTempsAll(String tempsdebut, String tempsfin) {
        if (tempsdebut != null && !tempsdebut.isEmpty()) {
            setTempsdebut(Time.valueOf(tempsdebut + ":00"));
        }
        if (tempsfin != null && !tempsfin.isEmpty()) {
            setTempsfin(Time.valueOf(tempsfin + ":00"));
        }
    }

    public String getTempsPerso() {
        return "de " + tempsdebut + " à " + tempsfin;
    }

    public Intervalle getDisponible(GenericDao genericdao, Timestamp debut, Timestamp fin) throws Exception {
        List<Intervalle> interTrue = new ArrayList<>();
        List<ScenePersonnage> listescenepersonnage = genericdao.find(this, null, null, null, null, null);

        for (ScenePersonnage scenePersonnage : listescenepersonnage) {
            Personnage p = new Personnage();
            p.setId(scenePersonnage.getIdpersonnage());
            p = (Personnage) genericdao.findById(p);
            
            if (p.getPersonnageDisponible(genericdao, debut, fin).size() > interTrue.size()) {
                interTrue = p.getPersonnageDisponible(genericdao, debut, fin);
            }
        }
        
        
        for (Intervalle intervalle : interTrue) {
            if (isDisponible(genericdao, intervalle.getDebut(), intervalle.getFin())) {
                return intervalle;
            }
        }
        return null;
    }

    public boolean isDisponible(GenericDao genericdao, Timestamp debut, Timestamp fin) throws Exception {
        List<ScenePersonnage> listescenepersonnage = genericdao.find(this, null, null, null, null, null);
        int counter = 0;

        for (ScenePersonnage scenePersonnage : listescenepersonnage) {
            Personnage p = new Personnage();
            p.setId(scenePersonnage.getIdpersonnage());
            p = (Personnage) genericdao.findById(p);

            if (!p.getPersonnageDisponible(genericdao, debut, fin).isEmpty()) {
                counter++;
            }
        }
        if (counter == listescenepersonnage.size()) {
            return true;
        }
        return false;
    }

    public Intervalle isDisponibleIntervalle(GenericDao genericdao, Timestamp debut, Timestamp fin) throws Exception {
        List<Intervalle> result = new ArrayList<>();
        List<ScenePersonnage> listescenepersonnage = genericdao.find(this, null, null, null, null, null);
        int counter = 0;
        for (ScenePersonnage scenePersonnage : listescenepersonnage) {
            Personnage p = new Personnage();
            p.setId(scenePersonnage.getIdpersonnage());
            p = (Personnage) genericdao.findById(p);

            System.out.println("\n\n\ndebut = = = = = " + debut + " = = = = = fin = = = =" + fin);
            if (!p.getPersonnageDisponible(genericdao, debut, fin).isEmpty()) {
                result.add(p.getPersonnageDisponible(genericdao, debut, fin).get(0));
                System.out.println("personnage = = = = = = " + p.getNoms() + " = = = " + p.getPersonnageDisponible(genericdao, debut, fin));
                counter++;
            }
        }
        Collections.sort(result, Comparator.comparing(Intervalle::getDebut).reversed());
        System.out.println("result = = = = = = = = = = = = =" + result + "\n\n");
        return result.get(0);
    }

}
