/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.Getter;
import lombok.Setter;
import modelUtils.BaseModel;
import util.Column;
import util.Table;

/**
 *
 * @author charles
 */
@Getter
@Setter
@Table(name = "emotion")
public class Emotion extends BaseModel{
    @Column(name = "libelle")
    String libelle;
}
