/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package interfaceUtils;

import java.util.List;
import modelUtils.BaseModel;

/**
 *
 * @author charles
 */
public interface InterfaceDao {

    public void save(BaseModel model) throws Exception;

    public void update(BaseModel model, String[] setCondition) throws Exception;

    public void delete(BaseModel model) throws Exception;

    public List find(BaseModel model, String[] elasticSearchFields, String elasticSearchValue, String[] orderFields, String[] orderValues, String[] whereString,Integer... args) throws Exception;

    public BaseModel findById(BaseModel model) throws Exception;

    public long countRow(BaseModel model) throws Exception;

    public long countRow(String query) throws Exception;

}
