/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import interfaceUtils.InterfaceDao;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import lombok.Setter;
import modelUtils.BaseModel;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import util.Column;
import util.Function;

/**
 *
 * @author elsys
 */
@Setter
public class GenericDao implements InterfaceDao {

    @Autowired
    private BasicDataSource basicDataSource;

    private void setObjects(PreparedStatement preparedStatement, List values) throws Exception {
        int i = 1;
        for (Object value : values) {
            if (value != null) {
                try {
                    preparedStatement.setObject(i, value);
                    i++;
                } catch (Exception ex) {
                    throw ex;
                }
            }
        }
    }

    @Override
    public void save(BaseModel model) throws Exception {
        Connection connection = basicDataSource.getConnection();

        List<Column> attributesName = null;
        List<Object> attributesValue = null;
        PreparedStatement ps = null;
        String query = null;
        List<String> temp = null;

        try {
            attributesName = Function.getAttributesName(model);
            attributesValue = Function.getAttributesValue(model);
            query = "insert into " + Function.getTableName(model) + "(";
            int i = 0, count = 0;
            temp = new ArrayList<>();
            for (; i < attributesName.size(); i++) {
                if (attributesValue.get(i) != null) {
                    temp.add(attributesName.get(i).name());
                    count++;
                }
            }
            if (!temp.isEmpty()) {
                int a = 0;
                for (; a < temp.size() - 1; a++) {
                    query = query + temp.get(a) + ", ";
                }
                query = query + temp.get(a);
            }
            query = query + ") values (";
            if (!temp.isEmpty()) {
                i = 0;
                for (; i < count - 1; i++) {
                    query = query + "?, ";
                }
                query = query + "?";
            }
            query = query + ")";
            ps = connection.prepareStatement(query);
            setObjects(ps, attributesValue);
            int rs = ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

    }

    public List<HashMap> executeQuery(String query, String[] columns) throws Exception {
        Connection connection = basicDataSource.getConnection();
        PreparedStatement ps = connection.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        List<HashMap> trueresult = new ArrayList<HashMap>();
        while (rs.next()) {
            HashMap result = new HashMap();
            for (String column : columns) {
                if (rs.getObject(column) != null) {
                    result.put(column, rs.getObject(column));
                }
            }
            trueresult.add(result);
        }
        // close
        rs.close();
        ps.close();
        connection.close();
        return trueresult;
    }

    public void executeQueryNotReturned(String query) throws Exception {
        Connection connection = basicDataSource.getConnection();
        PreparedStatement ps = connection.prepareStatement(query);
        int rs = ps.executeUpdate();
        // close
        ps.close();
        connection.close();
    }

  //Niova BASEMODEL
    public List executeQuery(BaseModel baseModel, String query) throws Exception {
        Connection connection = basicDataSource.getConnection();
        PreparedStatement ps = connection.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        List trueresult = new ArrayList();
        while (rs.next()) {
            Object object1 = baseModel.getClass().newInstance();
            for (Field column : Function.getAllFields(baseModel)) {
                if (Function.getAttributeName(column) != null && Function.isExist(rs, column.getName()) && rs.getObject(column.getName()) != null) {
                    Function.setObjectValue(object1, column, rs.getObject(column.getName()));
                }
            }
            trueresult.add(object1);
        }
        // close
        rs.close();
        ps.close();
        connection.close();
        return trueresult;
    }


    public Long getNextSequenceValue(String sequenceName) throws Exception {
        List<HashMap> seq = executeQuery("select nextval('" + sequenceName + "')", new String[]{"nextval"});
        return (Long) seq.get(0).get("nextval");
    }

    @Override
    public List find(BaseModel model, String[] elasticSearchFields, String elasticSearchValue, String[] orderFields, String[] orderValues, String[] whereString, Integer... args) throws Exception {
        Connection connection = basicDataSource.getConnection();
        List<Object> attributesValue = null;
        List<Object> trueAttributeValue = null;
        List<Column> attributesName = null;
        String tableName = null;
        String query = null;
        List<Object> result = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            attributesValue = Function.getAttributesValue(model);
            trueAttributeValue = new ArrayList<>();
            attributesName = Function.getAttributesName(model);
            tableName = Function.getTableName(model);
            query = "select ";
            int i = 0;
            for (; i < attributesName.size() - 1; i++) {
                query = query + attributesName.get(i).name() + ", ";
            }
            query = query + attributesName.get(i).name();
            query = query + " from " + tableName + " where 0=0 ";

            if ((elasticSearchFields == null || elasticSearchFields.length == 0) || (elasticSearchValue == null || elasticSearchValue.isEmpty())) {
                i = 0;
                for (; i < attributesValue.size(); i++) {
                    if (attributesValue.get(i) != null) {
                        query = query + " and " + attributesName.get(i).name() + "= ? ";
                        trueAttributeValue.add(attributesValue.get(i));
                    }
                }
            } else {
                String fieldSearch = elasticSearchFields[0];
                for (int j = 1; j < elasticSearchFields.length; j++) {
                    fieldSearch = fieldSearch + "||' '||" + elasticSearchFields[j];
                }
                String valueSearch = "'";
                String[] splittedelasticSearchValue = elasticSearchValue.split(" ");
                int j = 0;
                for (; j < splittedelasticSearchValue.length - 1; j++) {
                    valueSearch = valueSearch + "%" + splittedelasticSearchValue[j] + "% ";
                }
                valueSearch = valueSearch + "%" + splittedelasticSearchValue[j] + "%'";
                query = query + " and " + fieldSearch + " ilike " + valueSearch;
            }

            ///
            if (whereString != null && whereString.length != 0) {
                for (String string : whereString) {
                    query = query + " and " + string;
                }
            }
            ///

            ///
            if (orderFields != null && orderFields.length != 0 && orderValues != null && orderValues.length != 0) {
                query = query + " order by ";
                query = query + orderFields[0] + " " + orderValues[0];
                for (int j = 1; j < orderFields.length; j++) {
                    query = query + ", " + orderFields[j] + " " + orderValues[j];
                }
            }
            ///

            ///
            if (args.length == 2 && args[0] != null && args[1] != null) {
                query = query + " limit " + (args[1]) + " offset " + (args[0] - 1);
            }
            ///
            ps = connection.prepareStatement(query);
            setObjects(ps, trueAttributeValue);
            rs = ps.executeQuery();
            Field[] fields = Function.getAllFields(model);
            result = new ArrayList<>();
            while (rs.next()) {
                Object object1 = model.getClass().newInstance();
                for (Field field : fields) {
                    if (Function.getAttributeName(field) != null) {
                        Function.setObjectValue(object1, field, rs.getObject(Function.getAttributeName(field).name()));
                    }
                }
                result.add(object1);
            }
            return result;

        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    public String findQuery(BaseModel model, String[] elasticSearchFields, String elasticSearchValue, String[] orderFields, String[] orderValues, String[] whereString, Integer... args) throws Exception {
        List<Object> attributesValue = null;
        List<Object> trueAttributeValue = null;
        List<Column> attributesName = null;
        String tableName = null;
        String query = null;
        List<Object> result = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        attributesValue = Function.getAttributesValue(model);
        trueAttributeValue = new ArrayList<>();
        attributesName = Function.getAttributesName(model);
        tableName = Function.getTableName(model);
        query = "select ";
        int i = 0;
        for (; i < attributesName.size() - 1; i++) {
            query = query + attributesName.get(i).name() + ", ";
        }
        query = query + attributesName.get(i).name();
        query = query + " from " + tableName + " where 0=0 ";

        if ((elasticSearchFields == null || elasticSearchFields.length == 0) || (elasticSearchValue == null || elasticSearchValue.isEmpty())) {
            i = 0;
            for (; i < attributesValue.size(); i++) {
                if (attributesValue.get(i) != null) {
                    query = query + " and " + attributesName.get(i).name() + "= ? ";
                    trueAttributeValue.add(attributesValue.get(i));
                }
            }
        } else {
            String fieldSearch = elasticSearchFields[0];
            for (int j = 1; j < elasticSearchFields.length; j++) {
                fieldSearch = fieldSearch + "||' '||" + elasticSearchFields[j];
            }
            String valueSearch = "'";
            String[] splittedelasticSearchValue = elasticSearchValue.split(" ");
            int j = 0;
            for (; j < splittedelasticSearchValue.length - 1; j++) {
                valueSearch = valueSearch + "%" + splittedelasticSearchValue[j] + "% ";
            }
            valueSearch = valueSearch + "%" + splittedelasticSearchValue[j] + "%'";
            query = query + " and " + fieldSearch + " ilike " + valueSearch;
        }

        ///
        if (whereString != null && whereString.length != 0) {
            for (String string : whereString) {
                query = query + " and " + string;
            }
        }
        ///

        ///
        if (orderFields != null && orderFields.length != 0 && orderValues != null && orderValues.length != 0) {
            query = query + " order by ";
            query = query + orderFields[0] + " " + orderValues[0];
            for (int j = 1; j < orderFields.length; j++) {
                query = query + ", " + orderFields[j] + " " + orderValues[j];
            }
        }
        ///

        ///
        if (args.length == 2 && args[0] != null && args[1] != null) {
            query = query + " limit " + (args[1]) + " offset " + (args[0] - 1);
        }
        System.out.println(query);

        return query;
    }

    public void update(BaseModel model, String[] names, String[] setCondition) throws Exception {
        Connection connection = basicDataSource.getConnection();
        List<Column> attributesName = null;
        List<Object> attributesValue = null;
        List temp = null;
        String query = null;
        PreparedStatement ps = null;

        try {
            attributesName = Function.getAttributesName(model);
            attributesValue = Function.getAttributesValue(model);
            temp = new ArrayList();
            query = "update " + Function.getTableName(model) + " set ";
            int i = 0;
            for (; i < attributesName.size(); i++) {
                if (attributesValue.get(i) != null) {
                    temp.add(attributesName.get(i).name());
                }
            }
            if (!temp.isEmpty()) {
                int k = 0;
                for (; k < temp.size() - 1; k++) {
                    query = query + temp.get(k) + "=?, ";
                }
                query = query + temp.get(k) + "=? ";
            }
            if (setCondition != null) {
                for (String string : setCondition) {
                    query = query + "," + string + " ";
                }                
            }
            query = query + " where 0=0";
            for (String name : names) {
                for (i = 0; i < attributesName.size(); i++) {
                    if (attributesValue.get(i) != null && attributesName.get(i).name().equals(name)) {
                        query = query + " and " + attributesName.get(i).name() + "=? ";
                    }
                }
            }
            ps = connection.prepareStatement(query);
            int index = 1;
            for (int j = 0; j < attributesValue.size(); j++) {

                if (attributesValue.get(j) != null) {
                    ps.setObject(index, attributesValue.get(j));
                    index++;
                }
            }
            for (String name : names) {
                for (i = 0; i < attributesName.size(); i++) {
                    if (attributesValue.get(i) != null && attributesName.get(i).name().equals(name)) {
                        ps.setObject(index, attributesValue.get(i));
                        index++;
                    }
                }
            }
            int rs = ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

    }

    public String updateQuery(BaseModel model, String[] names, String[] setCondition) throws Exception {
        Connection connection = basicDataSource.getConnection();
        List<Column> attributesName = null;
        List<Object> attributesValue = null;
        List temp = null;
        String query = null;
        PreparedStatement ps = null;

        try {
            attributesName = Function.getAttributesName(model);
            attributesValue = Function.getAttributesValue(model);
            temp = new ArrayList();
            query = "update " + Function.getTableName(model) + " set ";
            int i = 0;
            for (; i < attributesName.size(); i++) {
                if (attributesValue.get(i) != null) {
                    temp.add(attributesName.get(i).name());
                }
            }
            if (!temp.isEmpty()) {
                int k = 0;
                for (; k < temp.size() - 1; k++) {
                    query = query + temp.get(k) + "=?, ";
                }
                query = query + temp.get(k) + "=?";
            }
             if (setCondition != null) {
                for (String string : setCondition) {
                    query = query + "," + string + " ";
                }                
            }
            query = query + " where 0=0";
            for (String name : names) {
                for (i = 0; i < attributesName.size(); i++) {
                    System.out.println("attr = " + attributesName.get(i).name() + " ------- name = " + name);
                    if (attributesValue.get(i) != null && attributesName.get(i).name().equals(name)) {
                        query = query + " and " + attributesName.get(i).name() + "=? ";
                    }
                }
            }
            
            ps = connection.prepareStatement(query);
            int index = 1;
            for (int j = 0; j < attributesValue.size(); j++) {

                if (attributesValue.get(j) != null) {
                    ps.setObject(index, attributesValue.get(j));
                    index++;
                }
            }
            for (String name : names) {
                for (i = 0; i < attributesName.size(); i++) {
                    if (attributesValue.get(i) != null && attributesName.get(i).name().equals(name)) {
                        ps.setObject(index, attributesValue.get(i));
                        index++;
                    }
                }
            }
            
            return query;
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

    }
    
    @Override
    public void update(BaseModel model, String[] setCondition) throws Exception {
        try {
            update(model, new String[]{"id"}, setCondition);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void delete(BaseModel model) throws Exception {
        Connection connection = basicDataSource.getConnection();
        List<Object> attributesValue = null;
        List<Column> attributesName = null;
        String query = null;
        PreparedStatement ps = null;

        try {
            attributesValue = Function.getAttributesValue(model);
            attributesName = Function.getAttributesName(model);
            query = "delete from " + Function.getTableName(model) + " where 0=0 ";
            int i = 0;
            for (; i < attributesName.size(); i++) {
                if (attributesValue.get(i) != null) {
                    query = query + "and " + attributesName.get(i).name() + "=? ";
                }
            }
            ps = connection.prepareStatement(query);
            setObjects(ps, attributesValue);
            int rs = ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

    }

    @Override
    public BaseModel findById(BaseModel model) throws Exception {
        BaseModel m = null;
        try {
            m = model.getClass().newInstance();
            m.setId(model.getId());
            return (BaseModel) find(m, null, null, null, null, null).get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public long countRow(BaseModel model) throws Exception {
        try {
            long result = (long) executeQuery("select count(*) from " + Function.getTableName(model), new String[]{"count"}).get(0).get("count");
            return result;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public long countRow(String query) throws Exception {
        try {
            long result = (long) executeQuery("select count(*) from  ( " + query + " ) temp", new String[]{"count"}).get(0).get("count");
            return result;
        } catch (Exception e) {
            throw e;
        }
    }

}
