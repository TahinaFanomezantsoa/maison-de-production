/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import dao.GenericDao;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.Personne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author charles
 */
@Controller
public class PersonneController {

    @Autowired
    GenericDao genericDao;

    @RequestMapping("/login")
    public ModelAndView login(ModelAndView mv, HttpServletRequest request) throws Exception {
        Personne personne = null;
        try {
            personne = new Personne();
            personne.setIdentifiant(request.getParameter("identifiant"));
            personne.setMotdepasse(request.getParameter("motdepasse"));
            List<Personne> listepersonne = genericDao.find(personne, null, null, null, null, null);
            if (listepersonne != null && !listepersonne.isEmpty()) {
                personne = listepersonne.get(0);
                request.getSession().setAttribute("id", personne.getId());
                request.getSession().setAttribute("noms", personne.getNoms());
                request.getSession().setAttribute("typepersonne", personne.getTypepersonneString());
                mv.setViewName("redirect:/?loginsucc");
            } else {
                mv.setViewName("redirect:/?loginerr");
            }

        } catch (Exception e) {
            throw e;
        }

        return mv;
    }

    @RequestMapping("/logout")
    public ModelAndView logout(ModelAndView mv, HttpServletRequest request) {
        request.getSession().removeAttribute("id");
        mv.setViewName("redirect:/?logoutsucc");
        return mv;
    }

}
