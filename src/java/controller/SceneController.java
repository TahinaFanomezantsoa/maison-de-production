/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import dao.GenericDao;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import model.DisponibilitePersonnage;
import model.DisponibilitePlateau;
import model.Emotion;
import model.Personnage;
import model.Scene;
import model.Personne;
import model.Plateau;
import model.ScenePersonnage;
import model.ScenePlannifie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author charles
 */
@Controller
public class SceneController {

    @Autowired
    GenericDao genericDao;

    List<ScenePersonnage> listscenepersonnage = new ArrayList<>();

    List<Scene> listesceneaplanifier = new Vector<>();

    List<Scene> resultataplanifier = new Vector<>();

    @RequestMapping({"/", "/listescene"})
    public ModelAndView listescene(ModelAndView mv, HttpServletRequest request) throws Exception {
        String page = request.getParameter("page");
        List<Integer> nombrepagination = Scene.getNombrepagination(genericDao, genericDao.findQuery(new Scene(), new String[]{"libelle"}, request.getParameter("searchvalue"), null, null, new String[]{"etat = 21"}));

        if (request.getSession().getAttribute("id") != null) {
            if (request.getSession().getAttribute("typepersonne").equals("Admin")) {
                nombrepagination = Scene.getNombrepagination(genericDao, genericDao.findQuery(new Scene(), new String[]{"libelle"}, request.getParameter("searchvalue"), null, null, null));

            } else if (request.getSession().getAttribute("typepersonne").equals("Auteur")) {
                nombrepagination = Scene.getNombrepagination(genericDao, genericDao.findQuery(new Scene(), new String[]{"libelle"}, request.getParameter("searchvalue"), null, null, new String[]{"idpersonne = '" + request.getSession().getAttribute("id") + "'"}));
            }

        }
        
        
        ///
        List<Scene> result1 = genericDao.find(new Scene(), null, null, null, null, null);
        for (Scene scene : result1) {
            System.out.println(scene.getId() + " ---- " + scene.getDuree());
        }
        ///
        
        

        int debut = 1;
        int size = Scene.getNombrecontenuparpage(genericDao);
        Integer debut1 = 1;
        if (page != null && !page.isEmpty()) {
            if (debut < nombrepagination.size()) {

                debut = Integer.parseInt(page);
                debut1 = (Integer.parseInt(page) * size) - size + 1;

            }
        }

        List<Scene> listescene = genericDao.find(new Scene(), new String[]{"libelle"}, request.getParameter("searchvalue"), new String[]{"ordre"}, new String[]{"asc"}, new String[]{"etat = 21"}, debut1, size);

        if (request.getSession().getAttribute("id") != null) {
            if (request.getSession().getAttribute("typepersonne").equals("Admin")) {
                listescene = genericDao.find(new Scene(), new String[]{"libelle"}, request.getParameter("searchvalue"), new String[]{"ordre"}, new String[]{"asc"}, null, debut1, size);

            } else if (request.getSession().getAttribute("typepersonne").equals("Auteur")) {
                listescene = genericDao.find(new Scene(), new String[]{"libelle"}, request.getParameter("searchvalue"), new String[]{"ordre"}, new String[]{"asc"}, new String[]{"idpersonne = '" + request.getSession().getAttribute("id") + "'"}, debut1, size);
            }

        }

        List<Plateau> listeplateau = new ArrayList<>();
        for (Scene scene : listescene) {
            Plateau p = new Plateau();
            p.setId(scene.getIdplateau());
            listeplateau.add((Plateau) genericDao.findById(p));
        }
        List<Personne> listepersonne = new ArrayList<>();
        for (Scene scene : listescene) {
            Personne p = new Personne();
            p.setId(scene.getIdpersonne());
            Personne result = (Personne) genericDao.findById(p);
            result.setMotdepasse(null);
            listepersonne.add(result);
        }

        request.setAttribute("listeplateauall", genericDao.find(new Plateau(), null, null, null, null, null));
        request.setAttribute("nombrecontenuparpage", size);
        request.setAttribute("listescene", listescene);
        request.setAttribute("listeplateau", listeplateau);
        request.setAttribute("listepersonne", listepersonne);
        request.setAttribute("nombrepagination", nombrepagination);
        request.setAttribute("listepersonnage", genericDao.find(new Personnage(), null, null, null, null, null));
        request.setAttribute("listeemotion", genericDao.find(new Emotion(), null, null, null, null, null));
        request.setAttribute("page", debut);
        mv.setViewName("listescene");
        return mv;
    }

    @GetMapping(path = "/valider")
    public String valider(@RequestParam String id) throws Exception {
        Scene scene = new Scene();
        scene.setId(id);
        scene.setEtat(21);
        scene.setDatevalidation(Timestamp.from(Instant.now()));
        String[] condition = new String[]{id};
        this.genericDao.update(scene, null);
        return "redirect:/listescene?scenevalidesucc";

    }

    @RequestMapping("/modifiernombrecontenuparpage")
    public ModelAndView modifiernombrecontenuparpage(ModelAndView mv, HttpServletRequest request) throws Exception {
        String nombrecontenuparpage = request.getParameter("nombrecontenuparpage");
        String page = request.getParameter("page");
        String searchvalue = request.getParameter("searchvalue");

        String link = "redirect:/listescene?modifcontenuparpagesucc";
        if (page != null && !page.isEmpty() && !page.equals("null")) {
            link = link + "&page=" + page;
        }
        if (searchvalue != null && !searchvalue.isEmpty() && !searchvalue.equals("null")) {
            link = link + "&searchvalue=" + searchvalue;
        }

        if (nombrecontenuparpage != null && !nombrecontenuparpage.isEmpty()) {
            Scene.updateContenuparpage(Integer.parseInt(nombrecontenuparpage), genericDao);
        }
        mv.setViewName(link);
        return mv;
    }

    @RequestMapping("/nouveauscene")
    public ModelAndView nouveauscene(ModelAndView mv, HttpServletRequest request) throws Exception {

        String action = request.getParameter("action");
        String dialogue = request.getParameter("dialogue");
        String idpersonnage = request.getParameter("idpersonnage");
        String idemotion = request.getParameter("idemotion  ");
        String tempsdebut = request.getParameter("tempsdebut");
        String tempsfin = request.getParameter("tempsfin");
        String idtodelete = request.getParameter("idtodelete");

        if (idtodelete != null) {

            Iterator<ScenePersonnage> it = listscenepersonnage.iterator();
            while (it.hasNext()) {
                ScenePersonnage s = it.next();
                if (s.getId().equals(idtodelete)) {
                    it.remove(); // safe removal using iterator
                }
            }
        }

        ScenePersonnage sp = new ScenePersonnage();
        if (tempsdebut != null && tempsfin != null && !tempsdebut.isEmpty() && !tempsfin.isEmpty() && action != null) {
            sp.setId("sp" + genericDao.getNextSequenceValue("scenepersonnage_seq"));
            sp.setAction(action);
            sp.setDialogue(dialogue);

            if (idpersonnage != null && !idpersonnage.isEmpty() && !"null".equals(idpersonnage)) {
                sp.setIdpersonnage(idpersonnage);
            }

            if (idemotion != null && !idemotion.isEmpty() && !"null".equals(idemotion)) {
                sp.setIdemotion(idemotion);
            }
            sp.setTempsAll(tempsdebut, tempsfin);
            listscenepersonnage.add(sp);
        }

        request.setAttribute("listeplateau", genericDao.find(new Plateau(), null, null, null, null, null));
        request.setAttribute("listepersonnage", genericDao.find(new Personnage(), null, null, null, null, null));
        request.setAttribute("listeemotion", genericDao.find(new Emotion(), null, null, null, null, null));

        request.setAttribute("listescenepersonnage", listscenepersonnage);

        List<Emotion> listeemotionscenepersonnage = new ArrayList<>();
        List<Personnage> listepersonnagescenepersonnage = new ArrayList<>();
        for (ScenePersonnage scenePersonnage : listscenepersonnage) {
            if (scenePersonnage.getIdemotion() != null && !scenePersonnage.getIdemotion().isEmpty()) {
                Emotion e = new Emotion();
                e.setId(scenePersonnage.getIdemotion());
                listeemotionscenepersonnage.add((Emotion) genericDao.findById(e));
            } else {
                listeemotionscenepersonnage.add(new Emotion());
            }

            if (scenePersonnage.getIdpersonnage() != null && !scenePersonnage.getIdpersonnage().isEmpty()) {
                Personnage p = new Personnage();
                p.setId(scenePersonnage.getIdpersonnage());
                listepersonnagescenepersonnage.add((Personnage) genericDao.findById(p));
            } else {
                listepersonnagescenepersonnage.add(new Personnage());
            }

        }

        request.setAttribute("listeemotionscenepersonnage", listeemotionscenepersonnage);
        request.setAttribute("listepersonnagescenepersonnage", listepersonnagescenepersonnage);

        mv.setViewName("nouveauscene");
        return mv;
    }

    @RequestMapping("ajouternouveauscene")
    public ModelAndView ajouternouveauscene(ModelAndView mv, HttpServletRequest request) {
        try {
            Scene scene = new Scene();
            String id = "c" + genericDao.getNextSequenceValue("scene_seq");
            scene.setId(id);
            scene.setIdpersonne((String) request.getSession().getAttribute("id"));

            //**************
            if ("".equals(request.getParameter("idplateau"))) {
                scene.setIdplateau(null);
                scene.setEtat(1);

            } else {
                scene.setIdplateau(request.getParameter("idplateau"));
                scene.setEtat(11);
            }
            //*****************
            scene.setLibelle(request.getParameter("titre"));
            scene.setTempsAll(request.getParameter("tempsdebut"), request.getParameter("tempsfin"));

            //new modif
            scene.setOrdre(Integer.valueOf(request.getParameter("ordre")));

            if (request.getSession().getAttribute("id") != null) {
                if (request.getSession().getAttribute("typepersonne").equals("Admin")) {
                    scene.setDatePlannification(request.getParameter("dateplannification" + " " + request.getParameter("tempsdebutplannification") + ":00"), request.getParameter("dateplannification" + " " + request.getParameter("tempsfinplannification") + ":00"));
                    scene.setDatevalidation(Timestamp.valueOf(LocalDateTime.now()));

                }
            }

            //change
            scene.setDatevalidation(null);
            genericDao.save(scene);

            for (ScenePersonnage scenePersonnage : listscenepersonnage) {
                scenePersonnage.setIdscene(id);
                genericDao.save(scenePersonnage);
            }
            listscenepersonnage = new ArrayList<>();

            mv.setViewName("redirect:/nouveauscene?ajoutsucc");
            return mv;

        } catch (Exception e) {
            mv.setViewName("redirect:/nouveauscene?ajouterror");
            return mv;
        }

    }

    //Controller Scène à plannifier
    @RequestMapping("plannification")
    public ModelAndView listesceneplannifier(ModelAndView mv, HttpServletRequest request) throws Exception {
        String page = request.getParameter("page");
        List<Integer> nombrepagination = ScenePlannifie.getNombrepagination(genericDao, genericDao.findQuery(new Scene(), new String[]{"libelle", "datedebutplannification", "datefinplannification"}, request.getParameter("searchvalue"), null, null, null));

        int debut = 1;
        int size = ScenePlannifie.getNombrecontenuparpage(genericDao);
        Integer debut1 = 1;
        if (page != null && !page.isEmpty()) {
            if (debut < nombrepagination.size()) {

                debut = Integer.parseInt(page);
                debut1 = (Integer.parseInt(page) * size) - size + 1;

            }
        }

        List<ScenePlannifie> liste = genericDao.find(new ScenePlannifie(), new String[]{"libelle", "datedebutplannification", "datefinplannification"}, request.getParameter("searchvalue"), null, null, null, debut1, size);

        Scene scene = new Scene();
        scene.setEtat(21);
        List<Scene> listscene = (List<Scene>) genericDao.find(scene, null, null, null, null, null);

        scene.setEtat(31);
        List<Scene> listsceneplanifier = (List<Scene>) genericDao.find(scene, null, null, null, null, null);

        request.setAttribute("listeplannification", liste);
        request.setAttribute("nombrepagination", nombrepagination);
        request.setAttribute("nombrecontenuparpage", size);
        request.setAttribute("page", debut);
        request.setAttribute("listscene", listscene);
        request.setAttribute("listsceneplanifier", listsceneplanifier);
        request.setAttribute("scenetoplanifier", listesceneaplanifier);
        mv.setViewName("planification");
        return mv;
    }

    //    modif
    @RequestMapping("detailscene")
    public ModelAndView detailsscene(ModelAndView mv, HttpServletRequest request) throws Exception {
        String idscene = request.getParameter("id");
        Scene scene = new Scene();
        scene.setId(idscene);
        scene = (Scene) genericDao.findById(scene);

        ScenePersonnage sp = new ScenePersonnage();
        sp.setIdscene(idscene);

        Personne personne = new Personne();
        personne.setId(scene.getIdpersonne());
        personne = (Personne) genericDao.findById(personne);

        Plateau plateau = new Plateau();
        plateau.setId(scene.getIdplateau());
        plateau = (Plateau) genericDao.findById(plateau);

        System.out.println("idscene = " + idscene + " personne " + personne.getNoms() + " plateau " + plateau.getLibelle() + " " + plateau.getDescription());

        List<ScenePersonnage> listescenepersonnage = (List<ScenePersonnage>) genericDao.find(sp, null, null, null, null, null);

        List<Personnage> listpersonnage = new ArrayList<>();
        List<Emotion> listemotion = new ArrayList<>();
        for (ScenePersonnage scenePersonnage : listescenepersonnage) {
            Personnage pers = new Personnage();
            pers.setId(scenePersonnage.getIdpersonnage());

            if (pers.getId() != null) {
                pers = (Personnage) genericDao.findById(pers);
            }

            Emotion emotion = new Emotion();
            emotion.setId(scenePersonnage.getIdemotion());

            if (emotion.getId() != null) {
                emotion = (Emotion) genericDao.findById(emotion);
            }

            listpersonnage.add(pers);
            listemotion.add(emotion);

        }

        request.setAttribute("scene", scene);
        request.setAttribute("personne", personne);
        request.setAttribute("plateau", plateau);
        request.setAttribute("listescenepersonnage", listescenepersonnage);
        request.setAttribute("listpersonnage", listpersonnage);
        request.setAttribute("listemotion", listemotion);

        mv.setViewName("detailscene");

        return mv;
    }

    //MODIF VAOVAO MANDRESY//
    @RequestMapping("disponibilite")
    public ModelAndView disponibilite(ModelAndView mv, HttpServletRequest request) throws Exception {

        List<Personnage> listpersonnage = genericDao.executeQuery(new Personnage(), "select * from personnage");
        List<Plateau> plateau = genericDao.executeQuery(new Plateau(), "select * from plateau");
        request.setAttribute("listeplateau", plateau);
        request.setAttribute("listepersonnage", listpersonnage);

        mv.setViewName("disponibilite");

        return mv;
    }

    @RequestMapping("ajoutplateaudisponible")
    public ModelAndView ajoutPlateauDisponible(ModelAndView mv, HttpServletRequest request) throws Exception {

        String id = "dp" + genericDao.getNextSequenceValue(" disponibiliteplateau_seq");

        DisponibilitePlateau dp = new DisponibilitePlateau();
        dp.setId(id);
        dp.setIdplateau(request.getParameter("idplateau"));
        String[] debut = request.getParameter("tempsdebut").split("T");
        String[] fin = request.getParameter("tempsfin").split("T");
        dp.setDatedebut(Timestamp.valueOf(debut[0] + " " + debut[1] + ":00"));
        dp.setDatefin(Timestamp.valueOf(fin[0] + " " + fin[1] + ":00"));

        if (request.getSession().getAttribute("id") != null) {
            if (request.getSession().getAttribute("typepersonne").equals("Admin")) {
                genericDao.save(dp);
            }
        }

        mv.setViewName("redirect:/disponibilite?ajoutsucc");
        return mv;
    }

    @RequestMapping("ajoutacteurdisponible")
    public ModelAndView ajoutActeurDisponible(ModelAndView mv, HttpServletRequest request) throws Exception {
        String id = "da" + genericDao.getNextSequenceValue(" disponibilitepersonnage_seq");

        DisponibilitePersonnage da = new DisponibilitePersonnage();
        da.setId(id);
        da.setIdpersonnage(request.getParameter("idpersonnage"));
        String[] debut = request.getParameter("tempsdebut").split("T");
        String[] fin = request.getParameter("tempsfin").split("T");
        da.setDatedebut(Timestamp.valueOf(debut[0] + " " + debut[1] + ":00"));
        da.setDatefin(Timestamp.valueOf(fin[0] + " " + fin[1] + ":00"));

        if (request.getSession().getAttribute("id") != null) {
            if (request.getSession().getAttribute("typepersonne").equals("Admin")) {
                genericDao.save(da);
            }
        }
        mv.setViewName("redirect:/disponibilite?ajoutsucc");
        return mv;
    }

    @RequestMapping("planification/{idscene}")
    @ResponseBody
    public String ajoutersceneaplanifier(@PathVariable("idscene") String idscene, HttpServletRequest request) throws Exception {
        Scene scene = new Scene();
        scene.setId(idscene);
        scene = (Scene) genericDao.findById(scene);
        listesceneaplanifier.add(scene);
        return "ok";
    }

    @RequestMapping("planification/scene/fafana")
    @ResponseBody
    public String deleteallsceneaplanifier(HttpServletRequest request) throws Exception {
        listesceneaplanifier = new Vector<>();
        return "ok";
    }

    @RequestMapping("planification/scenes")
    @ResponseBody
    public String listsceneaplanifier() throws Exception {
        String html = "";
        for (Scene scene : listesceneaplanifier) {
            html += "<div class=\"col-sm-8\">\n";
            html += "<input readonly=\"\" class=\"form-control\" value=\"" + scene.getLibelle() + "\" />\n";
            html += "</div><br>";
        }
        return html;
    }

    @RequestMapping("plannifierscene")
    public ModelAndView planifierscene(ModelAndView mv,RedirectAttributes redirectAttributes,HttpServletRequest request) throws Exception {

        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

        Timestamp datedebut = new Timestamp(sf.parse(request.getParameter("datedebut")).getTime());
        Timestamp datefin = new Timestamp(sf.parse(request.getParameter("datefin")).getTime());

        System.out.println("liste scene = " + listesceneaplanifier);

        for (Scene scene : listesceneaplanifier) {
            System.out.println("scene a planifier " + scene.toString());
        }

        List<Scene> result = Scene.getSceneDisponibleOrdered(datedebut, datefin, genericDao, listesceneaplanifier);

        for (Scene scene : result) {
            System.out.println("resultat " + result);
        }

        System.out.println("size result " + result.size());

        resultataplanifier = result;

        request.getSession().setAttribute("resultat", result);
//        request.setAttribute("resultat",resultataplanifier);
//        redirectAttributes.addFlashAttribute("resultat",resultataplanifier);
//        mv.addObject("resultat",result);
        mv.setViewName("redirect:/plannification");

        return mv;

    }

    @RequestMapping("validerplanification")
    public ModelAndView validerplanification(ModelAndView mv, HttpServletRequest request) throws Exception {
        String[] idscenes = request.getParameterValues("idscenes");
        List<Scene> scenetoremoveinresult = new Vector<>();
        List<String> idscenestolist = Arrays.asList(idscenes);
        for (Scene scene : resultataplanifier) {
            if (idscenestolist.contains(scene.getId())) {
                scene.setEtat(31);
                genericDao.update(scene,null);
                scenetoremoveinresult.add(scene);
            }
        }
        for (Scene scene : scenetoremoveinresult) {
            resultataplanifier.remove(scene);
        }
        mv.setViewName("redirect:/plannification");
        return mv;
    }

    //MODIF VAOVAO MANDRESY//
//    fin
//
//    @RequestMapping("modifiercontenu")
//    public ModelAndView modifiercontenu(@RequestParam("file") MultipartFile file, ModelAndView mv, HttpServletRequest request) throws Exception {
//
//        Contenu contenu = new Contenu();
//        contenu.setId(request.getParameter("id"));
//        contenu.setIdtypecontenu(request.getParameter("idtypecontenu"));
//        contenu.setTitre(request.getParameter("titre"));
//        contenu.setDescription(request.getParameter("description"));
//        contenu.setLieu(request.getParameter("lieu"));
//        contenu.setDateAll(request.getParameter("date1"), request.getParameter("date2"));
//        if (file.getBytes().length != 0) {
//            contenu.setDataimage(file.getBytes());
//            contenu.setLibelleimage(file.getOriginalFilename());
//        }
//        contenu.setEtathome(Boolean.valueOf(request.getParameter("etathome")));
//        contenu.setDatepublicationPerso(request.getParameter("datepublication"));
//
//        String etatvalide = request.getParameter("etatvalide");
//        String[] setCondition = null;
//
//        if (request.getSession().getAttribute("typepersonne").equals("Auteur")) {
//            setCondition = new String[]{"datevalidation=null"};
//        }
//
//        if (etatvalide != null && !etatvalide.isEmpty()) {
//            if (etatvalide.equals("true")) {
//                contenu.setDatevalidation(Timestamp.valueOf(LocalDateTime.now()));
//            } else {
//                setCondition = new String[]{"datevalidation=null"};
//            }
//        }
//
//        genericDao.update(contenu, new String[]{"id"}, setCondition);
//
//        mv.setViewName("redirect:/listescene?modifsucc");
//        return mv;
//    }
}
