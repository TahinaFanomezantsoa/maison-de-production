
<%@page import="model.Personnage"%>
<%@page import="model.Emotion"%>
<%@page import="model.Personne"%>
<%@page import="model.Plateau"%>
<%@page import="model.Scene"%>
<%@page import="java.util.List"%>
<%
    List<Personnage> listepersonnage = (List<Personnage>) request.getAttribute("listepersonnage");
    List<Scene> listescene = (List<Scene>) request.getAttribute("listescene");
    List<Plateau> listeplateau = (List<Plateau>) request.getAttribute("listeplateau");
    List<Personne> listepersonne = (List<Personne>) request.getAttribute("listepersonne");
    List<Plateau> listeplateauall = (List<Plateau>) request.getAttribute("listeplateauall");
    List<Integer> nombrepagination = (List<Integer>) request.getAttribute("nombrepagination");
    int page1 = (int) request.getAttribute("page");
    String searchvalue = request.getParameter("searchvalue");
    String id = (String) request.getSession().getAttribute("id");
    int nombrecontenuparpage = (int) request.getAttribute("nombrecontenuparpage");
    List<Emotion> listeemotion = (List<Emotion>) request.getAttribute("listeemotion");
%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />

        <title>Liste</title>
        <meta content="" name="description" />
        <meta content="" name="keywords" />

        <!-- Favicons -->
        <link href="assets/img/favicon.png" rel="icon" />
        <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon" />

        <!-- Google Fonts -->
        <link href="https://fonts.gstatic.com" rel="preconnect" />
        <link
            href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
            rel="stylesheet"
            />

        <!-- Vendor CSS Files -->
        <link
            href="assets/vendor/bootstrap/css/bootstrap.min.css"
            rel="stylesheet"
            />
        <link
            href="assets/vendor/bootstrap-icons/bootstrap-icons.css"
            rel="stylesheet"
            />
        <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet" />
        <link href="assets/vendor/quill/quill.snow.css" rel="stylesheet" />
        <link href="assets/vendor/quill/quill.bubble.css" rel="stylesheet" />
        <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet" />
        <link href="assets/vendor/simple-datatables/style.css" rel="stylesheet" />

        <!-- Template Main CSS File -->
        <link href="assets/css/style.css" rel="stylesheet" />
    </head>

    <style>
        @media screen and (min-width: 676px) {
            .big {
                max-width: 75%; /* New width for default modal */
            }
            .small {
                max-width: 20%; /* New width for default modal */
            }
        }
        .scrollable {
            max-height: calc(100vh - 200px);
            overflow-y: auto;
        }
        a.logout:hover {
            color: rgba(255, 0, 0, 0.596);
        }
        a.login:hover {
            color: #9e9ae4;
        }
    </style>

    <body>

        <div class="modal fade" id="nouveauScene" role="dialog" tabindex="-1">
            <div class="modal-dialog small" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Connectez �votre compte</h5>
                        <button
                            type="button"
                            class="btn-close"
                            data-bs-dismiss="modal"
                            aria-label="Close"
                            ></button>
                    </div>
                    <div class="modal-body">
                        <form class="row g-3 needs-validation" novalidate method="post" action="login">
                            <div class="col-12">
                                <label for="identifiant" class="form-label">Identifiant</label>
                                <div class="input-group has-validation">
                                    <input
                                        type="text"
                                        name="identifiant"
                                        class="form-control"
                                        id="identifiant"
                                        required=""
                                        autocomplete="off"
                                        />
                                    <div class="invalid-feedback">
                                        Entrer une identifiant valide!
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <label for="motdepasse" class="form-label">Mot de passe</label>
                                <input
                                    type="password"
                                    name="motdepasse"
                                    class="form-control"
                                    id="motdepasse"
                                    required=""
                                    autocomplete="off"
                                    />
                                <div class="invalid-feedback">
                                    Entrer un mot de passe valide!
                                </div>
                            </div>

                            <div class="col-12">
                                <button class="btn btn-primary" type="submit">
                                    Se connecter
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>






        <div class="modal fade" id="modificationScene" role="dialog" tabindex="-1">
            <div class="modal-dialog big" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Modification de sc�ne</h4>
                        <button
                            type="button"
                            class="btn-close"
                            data-bs-dismiss="modal"
                            aria-label="Close"
                            ></button>
                    </div>
                    <div class="modal-body scrollable">
                        <form action="modifierscene" method="post">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title"></h5>

                                    <!-- General Form Elements -->

                                    <div class="row mb-3">
                                        <label for="inputText" class="col-sm-2 col-form-label">Titre</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="titre" required="" class="form-control">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label class="col-sm-2 col-form-label">Plateau</label>
                                        <div class="col-sm-10">
                                            <select name="idplateau" class="form-select" required="" id="mySelect" aria-label="Default select example">
                                                <% for (Plateau plateau : listeplateauall) {%>
                                                <option value="<%= plateau.getId()%>"> <%= plateau.getLibelle() + " " + plateau.getDescription()%> </option>
                                                <%}%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="inputDate" class="col-sm-2 col-form-label">Temps</label><div class="col-sm-5"><input name="tempsdebut" required="" type="time" class="form-control"></div><div class="col-sm-5"><input name="tempsfin" required="" type="time" class="form-control"></div>
                                    </div>


                                    <% if (id != null && !id.equals("null") && request.getSession().getAttribute("typepersonne") != null && request.getSession().getAttribute("typepersonne").equals("Admin")) {%>
                                    <div class="row mb-3" ><label for="inputDate" class="col-sm-2 col-form-label">Date de plannification</label><div class="col-sm-10"><input name="dateplannification" required="" type="date" class="form-control"></div>
                                    </div>


                                    <div class="row mb-3" ><label for="inputDate" class="col-sm-2 col-form-label">Temps</label><div class="col-sm-5"><input name="tempsdebutplannification" required="" type="time" class="form-control"></div><div class="col-sm-5"><input name="tempsfinplannification" required="" type="time" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label class="col-sm-2 col-form-label">Valid�</label>
                                        <div class="col-sm-10">
                                            <select required="" name="etatvalide" class="form-select" id="etatvalide" aria-label="Default select example">
                                                <option value="true">Oui</option>
                                                <option value="false">Non</option>
                                            </select>
                                        </div>
                                    </div>
                                    <%}%>
                                </div>
                            </div>

                            <div class="row align-items-top">

                                <div class="col-lg-6">
                                    <div class="card">
                                        <div class="card-body">

                                            <h5 class="card-title">Personne / �v�nement dans le sc�ne</h5>

                                            <!-- General Form Elements -->

                                            <div class="row mb-3">
                                                <label for="inputText" class="col-sm-2 col-form-label">Action</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="action" required="" class="form-control">
                                                </div>
                                            </div>

                                            <div class="row mb-3">
                                                <label for="inputText" class="col-sm-2 col-form-label">Dialogue</label>
                                                <div class="col-sm-10">
                                                    <textarea type="text" name="dialogue" required="" class="form-control"></textarea>
                                                </div>
                                            </div>

                                            <div class="row mb-3">
                                                <label class="col-sm-2 col-form-label">Personnage</label>
                                                <div class="col-sm-10">
                                                    <select name="idplateau" class="form-select" required="" id="mySelect" aria-label="Default select example">
                                                        <option value="null"> Pas de personnage </option>
                                                        <% for (Personnage personnage : listepersonnage) {%>
                                                        <option value="<%= personnage.getId()%>"> <%= personnage.getNoms()%> </option>
                                                        <%}%>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row mb-3">
                                                <label class="col-sm-2 col-form-label">Emotion</label>
                                                <div class="col-sm-10">
                                                    <select name="idplateau" class="form-select" required="" id="mySelect" aria-label="Default select example">
                                                        <option value="null"> Pas de �motion </option>
                                                        <% for (Emotion emotion : listeemotion) {%>
                                                        <option value="<%= emotion.getId()%>"> <%= emotion.getLibelle()%> </option>
                                                        <%}%>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row mb-3">
                                                <label for="inputDate" class="col-sm-2 col-form-label">Temps</label><div class="col-sm-5"><input name="tempsdebut" required="" type="time" class="form-control"></div><div class="col-sm-5"><input name="tempsfin" required="" type="time" class="form-control"></div>
                                            </div>

                                            <div class="row mb-3">
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-primary">+</button>
                                                </div>
                                            </div>

                                            <div class="row mb-3">
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-primary">Modifier</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>


                                <div class="col-lg-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title"></h5>

                                            <!-- Default Table -->
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Action</th>
                                                        <th scope="col">Personnage</th>
                                                        <th scope="col">Emotion</th>
                                                        <th scope="col">Temps</th>
                                                        <th scope="col"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>

                                                        <td>Brandon Jacob</td>
                                                        <td>Designer</td>
                                                        <td>28</td>
                                                        <td>2016-05-25</td>
                                                        <th>
                                                            <button type="submit" class="btn btn-danger">-</button>
                                                        </th>
                                                    </tr>
                                                    <tr>

                                                        <td>Bridie Kessler</td>
                                                        <td>Developer</td>
                                                        <td>35</td>
                                                        <td>2014-12-05</td>
                                                        <th>
                                                            <button type="submit" class="btn btn-danger">-</button>
                                                        </th>
                                                    </tr>
                                                    <tr>

                                                        <td>Ashleigh Langosh</td>
                                                        <td>Finance</td>
                                                        <td>45</td>
                                                        <td>2011-08-12</td>
                                                        <th>
                                                            <button type="submit" class="btn btn-danger">-</button>
                                                        </th>
                                                    </tr>
                                                    <tr>

                                                        <td>Angus Grady</td>
                                                        <td>HR</td>
                                                        <td>34</td>
                                                        <td>2012-06-11</td>
                                                        <th>
                                                            <button type="submit" class="btn btn-danger">-</button>
                                                        </th>
                                                    </tr>
                                                    <tr>

                                                        <td>Raheem Lehner</td>
                                                        <td>Dynamic Division Officer</td>
                                                        <td>47</td>
                                                        <td>2011-04-19</td>
                                                        <th>
                                                            <button type="submit" class="btn btn-danger">-</button>
                                                        </th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- End Default Table Example -->
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form><!-- End General Form Elements -->
                    </div>
                </div>
            </div>
        </div>


        <!-- ======= Header ======= -->
        <header id="header" class="header fixed-top d-flex align-items-center">
            <div class="d-flex align-items-center justify-content-between">
                <a class="logo d-flex align-items-center">
                    <i class="bi bi-film"></i>
                    <span class="d-none d-lg-block">Maison de Prod</span>
                </a>
                <i class="bi bi-list toggle-sidebar-btn"></i>
            </div>
            <!-- End Logo -->

            <div class="search-bar">
                <form
                    class="search-form d-flex align-items-center"
                    method="GET"
                    action=""
                    >
                    <input hidden="" type="text" name="page" value="<%= page1%>">

                    <input
                        type="text"
                        name="searchvalue"
                        <% if (searchvalue
                                    != null && !searchvalue.isEmpty()) {
                                out.print("value=\"" + searchvalue + "\"");
                            } %>
                        placeholder="Entrer votre recherche"
                        title="Entrer votre recherche"
                        />
                    <button type="submit" title="Search">
                        <i class="bi bi-search"></i>
                    </button>
                </form>
            </div>
            <!-- End Search Bar -->

            <nav class="header-nav ms-auto">
                <ul class="d-flex align-items-center">
                    <li class="nav-item d-block d-lg-none">
                        <a class="nav-link nav-icon search-bar-toggle" href="#">
                            <i class="bi bi-search"></i>
                        </a>
                    </li>
                    <!-- End Search Icon-->

                    <% if (id
                                != null && !id.equals(
                                        "null")) {%>
                    <li class="nav-item dropdown pe-3">

                        <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
                            <!--<img src="assets/img/profile-img.jpg" alt="Profile" class="rounded-circle">-->
                            <span class="d-none d-md-block dropdown-toggle ps-2"><%= request.getSession().getAttribute("noms")%></span>
                        </a><!-- End Profile Iamge Icon -->

                        <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                            <li class="dropdown-header">
                                <h6><%= request.getSession().getAttribute("noms")%></h6>
                                <span><%= request.getSession().getAttribute("typepersonne")%></span>
                            </li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li class="dropdown-header">
                                <a class="dropdown-item d-flex align-items-center" href="logout">
                                    <i class="bi bi-box-arrow-right"></i>
                                    <span>Se d�connecter</span>
                                </a>
                            </li>

                        </ul><!-- End Profile Dropdown Items -->
                    </li><!-- End Profile Nav -->
                    <%}%>


                    <li>
                        <a class="nav-link nav-profile d-flex align-items-center pe-5 login" data-bs-toggle="modal" data-bs-target="#nouveauScene" href="#"  >
                            <!-- Se connecter si non connecter and use modal si connecter Se deconnecter and use link-->

                            <%if (id
                                        == null || id.equals(
                                                "null")) {%>
                            <span>Se connecter</span> 
                            <%}%>
                        </a>
                    </li>
                    <!-- End Profile Nav -->
                </ul>
            </nav>
            <!-- End Icons Navigation -->
        </header>
        <!-- End Header -->

        <!-- ======= Sidebar ======= -->
        <aside id="sidebar" class="sidebar">
            <ul class="sidebar-nav" id="sidebar-nav">
                <li class="nav-item">
                    <a
                        class="nav-link collapsed"
                        data-bs-target="#components-nav"
                        data-bs-toggle="collapse"
                        href="#"
                        >
                        <i class="bi bi-grid"></i></i><span>Sc�ne</span
                        ><i class="bi bi-chevron-down ms-auto"></i>
                    </a>
                    <ul
                        id="components-nav"
                        class="nav-content collapse"
                        data-bs-parent="#sidebar-nav"
                        >
                        <% if (id
                                    != null && !id.equals(
                                            "null")) { %>
                        <li>
                            <a href="nouveauscene">
                                <i class="bi bi-circle"></i><span>Nouveau</span>
                            </a>
                        </li>
                        <%}%>
                        <li>
                            <a href="listescene">
                                <i class="bi bi-circle"></i><span>Liste</span>
                            </a>
                        </li>

                        <li>
                            <!--LIEN DE PLANNIFICATION-->
                            <a href="plannification">
                                <i class="bi bi-circle"></i><span>Plannification</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- End Dashboard Nav -->


            </ul>
        </aside>
        <!-- End Sidebar-->

        <main id="main" class="main">

            <%if (request.getParameter(
                        "loginerr") != null) {%>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <i class="bi bi-exclamation-octagon me-1"></i>
                Erreur de connexion !
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>

            <%}%>
            <%if (request.getParameter(
                        "loginsucc") != null) {%>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <i class="bi bi-check-circle me-1"></i>
                Vous �tes connect� avec succ�s !
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            <%}%>




            <%if (request.getParameter(
                        "scenevalidesucc") != null) {%>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <i class="bi bi-check-circle me-1"></i>
                Valid� avec succ�s !
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            <%}%>
            <%if (request.getParameter(
                        "logoutsucc") != null) {%>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <i class="bi bi-check-circle me-1"></i>
                Vous �tes d�connect� avec succ�s !
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            <%}%>
            <%if (request.getParameter(
                        "modifsucc") != null) {%>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <i class="bi bi-check-circle me-1"></i>
                Contenu modifi� avec succ�s !
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>

            <%}%>

            <%if (request.getParameter("modifcontenuparpagesucc") != null) {%>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <i class="bi bi-check-circle me-1"></i>
                Nombre de contenu par page modifi� avec succ�s !
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            <%}%>

            <% if (id != null && !id.equals("null") && request.getSession().getAttribute("typepersonne") != null && request.getSession().getAttribute("typepersonne").equals("Admin")) {%>
            <div class="card">
                <div class="card-body">

                    <h5 class="card-title"></h5>

                    <form action="modifiernombrecontenuparpage" method="get" id="myForm">
                        <div class="row mb-3">
                            <input hidden="" type="text" name="page" value="<%= request.getParameter("page")%>">
                            <input hidden="" type="text" name="searchvalue" value="<%= request.getParameter("searchvalue")%>">
                            <label for="inputText" class="col-sm-2 col-form-label">Nombre de contenu par page</label>
                            <div class="col-sm-2">
                                <input onchange="submitForm()" value="<%= nombrecontenuparpage%>" type="number" name="nombrecontenuparpage" class="form-control" min="1">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <%}%>

            <div class="pagetitle">
                <h1>Les Sc�nes</h1>
            </div>




            <div class="row align-items-top">
                <% for (int i = 0;
                            i < listescene.size();
                            i++) {%>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"><%= listescene.get(i).getLibelle()%></h5>
                            <p class="card-text">
                                <b>Plateau : </b><%= listeplateau.get(i).getLibelle() + " " + listeplateau.get(i).getDescription()%><br>
                                <%= listescene.get(i).getDateString()%><br>
                                <b>Ordre : </b><%= listescene.get(i).getOrdre()%>
                            </p>

                            <!--modif-->
                            <p class="card-text">
                                <% if (id != null && !id.equals("null")) {%>
                                <a href="#" data-bs-toggle="modal" data-bs-target="#modificationScene" class="btn btn-primary modifclass" data-id="<%= listescene.get(i).getId()%>" data-titre="<%= listescene.get(i).getLibelle()%>" data-datedebut="<%= listescene.get(i).getTempsdebut()%>" data-datefin="<%= listescene.get(i).getTempsfin()%>" data-page="<%= page1%>" data-searchvalue="<%= searchvalue%>" data-idplateau="<%= listescene.get(i).getIdplateau()%>" >Modifier</a>
                                <%}%>
                                <a href="detailscene?id=<%= listescene.get(i).getId()%>" class="btn btn-primary modifclass" >Details</a>
                                <% if (id != null && !id.equals("null") && request.getSession().getAttribute("typepersonne") != null && request.getSession().getAttribute("typepersonne").equals("Admin")
                                        && listescene.get(i).getEtat() < 21) {%>
                                <a href="valider?id=<%= listescene.get(i).getId()%>" class="btn btn-primary modifclass" >Valider</a>
                                <% } %>
                            </p>
                            <!--fin-->


                        </div>
                    </div>
                </div>

                <%}%>
            </div>




            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"></h5>

                    <!-- Pagination with icons -->
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="?page=<% if (page1
                                            - 1 > 0) {
                                        out.print(page1 - 1);
                                    } else {
                                        out.print(page1);
                                    }  %><% if (searchvalue
                                                != null && !searchvalue.isEmpty()) {
                                            out.print("&searchvalue=" + searchvalue);
                                        }%>" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>

                            <% for (int nombre : nombrepagination) {%>
                            <li class="page-item <% if (nombre == page1) {
                                    out.print("active");
                                }%>"><a class="page-link" href="?page=<%= nombre%><% if (searchvalue != null && !searchvalue.isEmpty()) {
                                        out.print("&searchvalue=" + searchvalue);
                                    }%>"><%= nombre%></a></li>
                                <%}%>
                            <li class="page-item">
                                <a class="page-link" href="?page=<% if (page1
                                            + 1 <= nombrepagination.size()) {
                                        out.print(page1 + 1);
                                    } else {
                                        out.print(page1);
                                    } %><% if (searchvalue
                                                != null && !searchvalue.isEmpty()) {
                                            out.print("&searchvalue=" + searchvalue);
                                        }%>" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <!-- End Pagination with icons -->
                </div>
            </div>
        </main>
        <!-- End #main -->

        <!-- ======= Footer ======= -->
        <footer id="footer" class="footer">
            <div class="copyright">
                &copy; Copyright <strong><span>CMS Company</span></strong
                >. All Rights Reserved
            </div>
            <div class="credits">
                Designed by <a href="#">Yels</a>
            </div>
        </footer>
        <!-- End Footer -->

        <a
            href="#"
            class="back-to-top d-flex align-items-center justify-content-center"
            ><i class="bi bi-arrow-up-short"></i
            ></a>

        <!-- Vendor JS Files -->
        <script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/vendor/chart.js/chart.umd.js"></script>
        <script src="assets/vendor/echarts/echarts.min.js"></script>
        <script src="assets/vendor/quill/quill.min.js"></script>
        <script src="assets/vendor/simple-datatables/simple-datatables.js"></script>
        <script src="assets/vendor/tinymce/tinymce.min.js"></script>
        <script src="assets/vendor/php-email-form/validate.js"></script>

        <!-- Template Main JS File -->
        <script src="assets/js/main.js"></script>
        <script src="assets/js/jquery.js"></script>
        <script>


                                    var alertEl = document.querySelector('.alert');
                                    setTimeout(function () {
                                        if (alertEl !== null) {
                                            alertEl.classList.remove('show');
                                            alertEl.classList.add('d-none');
                                        }
                                    }, 1000);

                                    $(document).on("click", ".modifclass", function () {
                                        $('#preview').attr('src', $(this).data('imagedata'));
                                        $(".modal-body #libelle").val($(this).data('libelle'));
                                        $(".modal-body #id").val($(this).data('id'));
                                        $(".modal-body #page").val($(this).data('page'));
                                        $(".modal-body #idplateau").val($(this).data('idplateau'));

                                        $(".modal-body #datedebut").val($(this).data('datedebut'));
                                        $(".modal-body #datefin").val($(this).data('datefin'));




                                        var searchvalue = $(this).data('searchvalue');
                                        if (searchvalue !== null) {
                                            $(".modal-body #searchvalue").val(searchvalue);
                                        }
                                    });
                                    function submitForm() {
                                        document.getElementById("myForm").submit();
                                    }

        </script>
    </body>
</html>
